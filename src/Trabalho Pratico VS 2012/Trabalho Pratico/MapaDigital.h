/***************************************
* MapaDigital.h
*
* Created on 13 de Outubro de 2013, 18:30
*
* @author Rui Silva & Daniela Grams
***************************************/

#ifndef MapaDigital_ 
#define MapaDigital_ 

#include <deque>
#include <bitset>

using namespace std;

#include "library\graphStlPath.h"
#include "Teste.h"

#pragma region Class MapaDigital

/*Esta classe fica respons�vel pela cria��o do grafo e todas
as restantes funcionalidades herdadas de graphVertex e graphStl*/
class MapaDigital : public graphStlPath<PLocTourist, PPath>
{
private:
	Teste t;
	typedef list < graphVertex <PLocTourist, PPath> >::iterator vIterator;
	typedef list < graphEdge <PLocTourist, PPath> >::iterator eIterator;

	PPath getPath(const PLocTourist &vOrigin, const PLocTourist &vDestination);
	void mostraCaminho(int vi, const vector <int> &path, int &kms, float &tolls, int &time);
	void menorCaminho(const PLocTourist &ci, const PLocTourist &cf);
	void maiorCaminho(const PLocTourist &ci, const PLocTourist &cf);

	bool percursoCompleto(vIterator it1, bitset <MAX_VERTICES> &taken, stack <PLocTourist> &percurso);
	bool atualizaTempoAtualLocal(PLocTourist p, int &tempoEspera, int &tempoAtual);
	bool atualizaTempoAtualVia(PPath p, int &tempoAtual);
public:
	MapaDigital();
	MapaDigital(const MapaDigital & md);
	~MapaDigital();

	void mostraCaminho(stack<PLocTourist> caminho, int i, int o);
	void mostraCaminhoDet(stack<PLocTourist> caminho, int &kms, float &tolls, int &time, int i, int o);

	bool createGraph(Teste t);
	void write(ostream &o);
	void todosCaminhos(PLocTourist o, PLocTourist d);
	void maiorLigacao();
	void menorCaminhoKms(const PLocTourist &ci, const PLocTourist &cf);
	void menorCaminhoCusto(const PLocTourist &ci, const PLocTourist &cf);
	void maiorCaminhoEntre(const PLocTourist &ci, const PLocTourist &cf);
	int tempoNecessarioParaTodosOsLocais();
	queue<stack<PLocTourist>> todosOsPercursosInferior(PLocTourist s1, PLocTourist s2, int tempo);

	void percursoCompleto(PLocTourist s1);
	queue <stack <PLocTourist> > caminhosHistoricos();
	queue <stack <PLocTourist> > viagemPersonalizada(PLocTourist lI, int tempoSaida, list<PLocTourist> lList, queue <int> &tComeco, queue <int> &tVisitas, queue <int> &tDeslocacao, queue <int> &mSaida, queue <int> &tEspera);
	
};
#pragma endregion

#pragma region Constructores

MapaDigital::MapaDigital() : graphStlPath<PLocTourist, PPath>()
{
}

MapaDigital::MapaDigital(const MapaDigital & md) : graphStlPath<PLocTourist, PPath>()
{
}

MapaDigital::~MapaDigital()
{
}

#pragma endregion

#pragma region Grafo
//cria o grafo
bool MapaDigital::createGraph(Teste t)
{
	this->t = t;
	if (getInfinite().getKm() != 0) //se houver importacoes anteriores, apaga a informa��o do grafo
	{
		PPath p;
		setInfinite(p);
		setKeys(0);
		vlist.clear();
	}
	if (!t.isTesteEmpty())
	{
		vector<PPath> caminhos = t.getVecPaths();

		for (unsigned i = 0; i < caminhos.size(); i++)
		{
			PLocTourist aux1 = t.getLocTourist(caminhos.at(i).getPath().getlocTourist1());
			PLocTourist aux2 = t.getLocTourist(caminhos.at(i).getPath().getlocTourist2());
			addGraphEdge(caminhos.at(i), aux1, aux2);
			addGraphEdge(caminhos.at(i), aux2, aux1); //para criar o grafo bidirectional
		}
		cout << getInfinite().getPath().getTotalKms() / 2 << " total de quilometros lidos no grafo" << endl;
		return true;
	}
	else
	{
		cout << endl << "Sem informacao para criar o grafo! Adicione e/ou importe informacao para continuar" << endl;
		return false;
	}
}

//guarda a informacao do mapa/grafo digital como string
void MapaDigital::write(ostream &out)
{
	out << "Infinite: " << getInfinite().getPath().getTotalKms() << " total de quilometros lidos no grafo" << endl << endl;
	system("PAUSE");
	for (list < graphVertex <PLocTourist, PPath> >::const_iterator iPLocTourist = vlist.begin(); iPLocTourist != vlist.end(); iPLocTourist++)
	{
		out << *iPLocTourist << endl;
		system("PAUSE");
	}
}

//apresenta a informacao do mapa/grafo
ostream & operator<<(ostream &out, MapaDigital & mD)
{
	mD.write(out);
	return out;
}
#pragma endregion

#pragma region Grafo metodos

PPath MapaDigital::getPath(const PLocTourist &vOrigin, const PLocTourist &vDestination) {
	int keyOrigin, keyDest;
	PPath p;
	this->getVertexKeyByContent(keyOrigin, vOrigin);
	this->getVertexKeyByContent(keyDest, vDestination);
	this->getEdgeByVertexKeys(p, keyOrigin, keyDest);
	return p;
}

//dados dois locais turisticos descobre o caminho mais curto entre os dois
void MapaDigital::menorCaminho(const PLocTourist &ci, const PLocTourist &cf) {
	vector <int> path; vector <PPath> dist; int key;

	this->getVertexKeyByContent(key, cf);
	this->dijkstrasAlgorithm(ci, path, dist);
	int kms = 0, time = 0;
	float tolls = 0;
	cout << ci.getLocTourist().getName();
	mostraCaminho(key, path, kms, tolls, time);
	cout << endl << "Detalhes do percurso: " << kms << "kms ; " << tolls << "EUR, " << time << " Minutos" << endl;
}

//dados dois locais turisticos descobre o maior caminho entre os dois
void MapaDigital::maiorCaminho(const PLocTourist &ci, const PLocTourist &cf)
{
	queue < stack <PLocTourist> > q = distinctPaths(ci, cf);
	unsigned maior = 0;
	stack<PLocTourist> mL;
	while (!q.empty())
	{
		stack <PLocTourist> sp = q.front();
		if (maior < sp.size())
		{
			maior = sp.size();
			if (!mL.empty())
				mL.pop();
			mL = sp;
		}
		q.pop();
	}
	mostraCaminho(mL, 0, 1);
}

//Este m�todo serve para expor na consola o caminho que � dado como parametro
//sob o contentor stack<PLocTourist> caminho.
//Como pretendemos em fases diferentes do programa apresentar os caminhos de 
//diferentes formas, decidimos ent�o criar um sistema de true(1) ou false(0)
//para determinar como se quer apresentar a informa��o:
//
// - mostraCaminho(stack<PLocTourist> caminho,0,0) : exemplo de como se apresenta
//	 a informa��o neste formato ((..),0,0) :=> local1 -> (local1, local2) -> local2
//
// - mostraCaminho(stack<PLocTourist> caminho,0,1) : exemplo de como se apresenta
//	 a informa��o neste formato ((..),0,1) :=> local1 -> (nome da via) -> local2 
//
// - mostraCaminho(stack<PLocTourist> caminho,1,0) : exemplo de como se apresenta
//	 a informa��o neste formato ((..),1,0) :=> local1 -> (nome da via, quilometros, pre�o, tempo) -> local2
//
// - mostraCaminho(stack<PLocTourist> caminho,1,1) : exemplo de como se apresenta
//	 a informa��o neste formato ((..),1,1) :=> local1 -> (local1, local2, nome da via, quilometros, pre�o, tempo) -> local2
void MapaDigital::mostraCaminho(stack<PLocTourist> caminho, int i, int o)
{
	stack<PLocTourist> reverse;
	while (!caminho.empty())
	{
		reverse.push(caminho.top());
		caminho.pop();
	}

	while (!reverse.empty())
	{
		cout << reverse.top().getLocTourist().getName();
		if (reverse.size() != 1)
		{
			stack<PLocTourist> mS = reverse;
			mS.pop();
			PPath e = getPath(mS.top(), reverse.top());
			if (i == 0 && o == 0)
			if (reverse.top().getLocTourist().getName() != e.getPath().getlocTourist1())
				cout << " -> (" << e.getPath().getlocTourist2() << ", " << e.getPath().getlocTourist1() << ") -> ";
			else
				cout << " -> (" << e.getPath().getlocTourist1() << ", " << e.getPath().getlocTourist2() << ") -> ";

			if (i == 0 && o == 1)
				cout << " -> (" << e.getPath().getPathName() << ") -> ";

			if (i == 1 && o == 0)
				cout << " -> (" << e.getPath().getPathName() << ", " << e.getPath().getTotalKms() << ", "
				<< e.getPath().getTolls() << ", " << e.getPath().getAvTravelTime() << ") -> ";

			if (i == 1 && o == 1)
			if (reverse.top().getLocTourist().getName() != e.getPath().getlocTourist1())
				cout << " -> (" << e.getPath().getlocTourist2() << ", " << e.getPath().getlocTourist1()
				<< ", " << e.getPath().getPathName() << ", " << e.getPath().getTotalKms() << ", "
				<< e.getPath().getTolls() << ", " << e.getPath().getAvTravelTime() << ") -> ";
			else
				cout << " -> (" << e.getPath().getlocTourist1() << ", " << e.getPath().getlocTourist2()
				<< ", " << e.getPath().getPathName() << ", " << e.getPath().getTotalKms() << ", "
				<< e.getPath().getTolls() << ", " << e.getPath().getAvTravelTime() << ") -> ";
		}
		reverse.pop();
	}
}

//Este m�todo serve para expor na consola o caminho que � dado como parametro
//sob o contentor stack<PLocTourist> caminho. Tamb�m recebe como refer�ncia os 
//parametros relativos �s informa��es da via (quilometros, pre�o, tempo) e calcula.
//Como pretendemos em fases diferentes do programa apresentar os caminhos de 
//diferentes formas, decidimos ent�o criar um sistema de true(1) ou false(0)
//para determinar como se quer apresentar a informa��o:
//
// - mostraCaminho(stack<PLocTourist> caminho,0,0) : exemplo de como se apresenta
//	 a informa��o neste formato ((..),0,0) :=> local1 -> (local1, local2) -> local2
//
// - mostraCaminho(stack<PLocTourist> caminho,0,1) : exemplo de como se apresenta
//	 a informa��o neste formato ((..),0,1) :=> local1 -> (nome da via) -> local2 
//
// - mostraCaminho(stack<PLocTourist> caminho,1,0) : exemplo de como se apresenta
//	 a informa��o neste formato ((..),1,0) :=> local1 -> (nome da via, quilometros, pre�o, tempo) -> local2
//
// - mostraCaminho(stack<PLocTourist> caminho,1,1) : exemplo de como se apresenta
//	 a informa��o neste formato ((..),1,1) :=> local1 -> (local1, local2, nome da via, quilometros, pre�o, tempo) -> local2
void MapaDigital::mostraCaminhoDet(stack<PLocTourist> caminho, int &kms, float &tolls, int &time, int i, int o)
{
	stack<PLocTourist> reverse;
	while (!caminho.empty())
	{
		reverse.push(caminho.top());
		caminho.pop();
	}

	while (!reverse.empty())
	{
		cout << reverse.top().getLocTourist().getName();
		if (reverse.size() != 1)
		{
			stack<PLocTourist> mS = reverse;
			mS.pop();
			PPath e = getPath(mS.top(), reverse.top());
			
			kms += e.getKm();
			tolls += e.getTolls();
			time += e.getAvTime();

			if (i == 0 && o == 0)
				if (reverse.top().getLocTourist().getName() != e.getPath().getlocTourist1())
					cout << " -> (" << e.getPath().getlocTourist2() << ", " << e.getPath().getlocTourist1() << ") -> ";
				else
					cout << " -> (" << e.getPath().getlocTourist1() << ", " << e.getPath().getlocTourist2() << ") -> ";
			
			if (i == 0 && o == 1)
				cout << " -> (" << e.getPath().getPathName() << ") -> ";
			
			if (i == 1 && o == 0)
				cout << " -> (" << e.getPath().getPathName() << ", " << e.getPath().getTotalKms() << ", "
				<< e.getPath().getTolls() << ", " << e.getPath().getAvTravelTime() << ") -> ";
			
			if (i == 1 && o == 1)
				if (reverse.top().getLocTourist().getName() != e.getPath().getlocTourist1())
					cout << " -> (" << e.getPath().getlocTourist2() << ", " << e.getPath().getlocTourist1()
					<< ", " << e.getPath().getPathName() << ", " << e.getPath().getTotalKms() << ", "
					<< e.getPath().getTolls() << ", " << e.getPath().getAvTravelTime() << ") -> ";
				else
					cout << " -> (" << e.getPath().getlocTourist1() << ", " << e.getPath().getlocTourist2()
					<< ", " << e.getPath().getPathName() << ", " << e.getPath().getTotalKms() << ", "
					<< e.getPath().getTolls() << ", " << e.getPath().getAvTravelTime() << ") -> ";
		}
		reverse.pop();
	}
}

//dado um vector de informacao apresenta essa informacao, � recursiva
//(chamado pela funcao menorCaminho, apresenta o menor caminho entre dois locais turisticos) 
void MapaDigital::mostraCaminho(int vi, const vector <int> &path, int &kms, float &tolls, int &time) {
	if (path[vi] == -1)
		return;

	mostraCaminho(path[vi], path, kms, tolls, time);

	PPath e;	this->getEdgeByVertexKeys(e, path[vi], vi);
	PLocTourist c; this->getVertexContentByKey(c, vi);
	kms += e.getKm();
	tolls += e.getTolls();
	time += e.getAvTime();
	cout << " -> (" << e.getPath().getPathName() << ", " << e.getKm() << " Kms, " << e.getTolls() << " Euros, " << e.getAvTime() << " Minutos) -> " << c.getLocTourist().getName();
}

void MapaDigital::todosCaminhos(PLocTourist o, PLocTourist d)
{
	// Caminhos distintos
	queue < stack <PLocTourist> > q = distinctPaths(o, d);
	cout << "Caminhos entre \"" << o.getLocTourist().getName() << "\" e \"" << d.getLocTourist().getName() << "\": " << q.size() << endl; // Apresentado de forma invertida
	cout << "Caminhos:" << endl;
	while (!q.empty())
	{
		stack <PLocTourist> sp = q.front();
		cout << "--------------------------------------------------------------------------------";
		mostraCaminho(sp, 0, 1);
		q.pop();
		cout << endl;
	}
	cout << "--------------------------------------------------------------------------------";
}

void MapaDigital::maiorLigacao()  {
	PPath::setComparacaoKMS();
	list < graphVertex <PLocTourist, PPath> >::iterator itv;
	PPath maximo;

	for (itv = vlist.begin(); itv != vlist.end(); itv++)
	{
		list < graphEdge <PLocTourist, PPath> >::iterator ite;
		for (ite = itv->getAdjacenciesBegin(); ite != itv->getAdjacenciesEnd(); ite++) 
			if (ite->getEContent() > maximo) maximo = ite->getEContent();
	}

	cout << "Maior distancia de ligacao direta: " << maximo.getKm() << endl;
	for (itv = vlist.begin(); itv != vlist.end(); itv++)
	{
		list < graphEdge <PLocTourist, PPath> >::iterator ite;
		for (ite = itv->getAdjacenciesBegin(); ite != itv->getAdjacenciesEnd(); ite++) {
			if (ite->getEContent() == maximo) cout << "Ligacao com maior distancia de " << itv->getVContent() << " a " << ite->getVDestination()->getVContent() << endl;
		}
	}
}

void MapaDigital::menorCaminhoKms(const PLocTourist &ci, const PLocTourist &cf) {

	PPath::setComparacaoKMS();
	cout << "Menor distancia entre " << ci.getLocTourist().getName() << " e " << cf.getLocTourist().getName() << " : " << endl;
	menorCaminho(ci, cf);
}

void MapaDigital::menorCaminhoCusto(const PLocTourist &ci, const PLocTourist &cf) {

	PPath::setComparacaoCUSTO();
	cout << "Menor custo entre " << ci.getLocTourist().getName() << " e " << cf.getLocTourist().getName() << " : " << endl;
	menorCaminho(ci, cf);
}

void MapaDigital::maiorCaminhoEntre(const PLocTourist &ci, const PLocTourist &cf)
{
	cout << "Caminho entre " << ci.getLocTourist().getName() << " e " << cf.getLocTourist().getName()
		<< " que passa pelo maior numero de Locais Turisticos intermedios : " << endl;
	maiorCaminho(ci, cf);
}

int MapaDigital::tempoNecessarioParaTodosOsLocais()
{
	int tempo = 0;
	for (auto v : vlist)
		if (v.getVContent().isLocHist())
			tempo += dynamic_cast<LocTouristHist&> (v.getVContent().getLocTourist()).getAvTime();
	return tempo;
}

void MapaDigital::percursoCompleto(PLocTourist s1)
{
	bitset <MAX_VERTICES> taken; taken.set();
	for (int i = 0; i < this->getKeys(); i++) taken[i] = false;

	vIterator it1; this->getVertexIteratorByContent(it1, s1);

	stack <PLocTourist> percurso;
	if (!percursoCompleto(it1, taken, percurso))
		cout << "Nao e possivel criar um caminho que passe por todos os Locais a partir deste Local de partida" << endl;
	else
	{
		int kms = 0, time = 0;
		float tolls = 0;
		mostraCaminhoDet(percurso, kms, tolls, time, 0, 1);
		cout << endl << "No tempo: " << time << " minutos" << endl;
		cout << endl;
	}
}

bool MapaDigital::percursoCompleto(vIterator it1, bitset <MAX_VERTICES> &taken, stack <PLocTourist> &percurso)
{
	percurso.push(it1->getVContent()); taken[it1->getVKey()] = true;

	if (taken.all()) return true;

	for (eIterator ite = it1->getAdjacenciesBegin();
		ite != it1->getAdjacenciesEnd(); ite++) {
		vIterator itv = ite->getVDestination();
		if (!taken[itv->getVKey()]) {

			bool saida = percursoCompleto(itv, taken, percurso);
			if (saida) return true;
		}
	}

	percurso.pop(); taken[it1->getVKey()] = false;
	return false;
}

queue < stack <PLocTourist> > MapaDigital::todosOsPercursosInferior(PLocTourist l1, PLocTourist l2, int tempo)
{
	queue < stack <PLocTourist> > caminhosDistintos = distinctPaths(l1, l2);

	queue < stack <PLocTourist> > caminhosComTempo;

	while (!caminhosDistintos.empty()) {
		stack <PLocTourist> sc = caminhosDistintos.front();
		bool temLocNat = false;
		int tempoCaminho = 0;

		while (!sc.empty()) {
			if (!sc.top().isLocHist())
			{
				temLocNat = true;
				tempoCaminho += sc.top().getLocTourist().getAvTime();
			}
			sc.pop();
		}

		if (tempoCaminho<tempo && !temLocNat)
			caminhosComTempo.push(caminhosDistintos.front());
		caminhosDistintos.pop();
	}
	return caminhosComTempo;
}

bool MapaDigital::atualizaTempoAtualLocal(PLocTourist p, int &tempoEspera, int &tempoAtual)
{
	if (tempoAtual == 0)
	{
		tempoAtual = p.getLocTourist().getOpenTime();
		tempoAtual += p.getLocTourist().getAvTime();
		return true;
	}
	else
	{
		if (tempoAtual > p.getLocTourist().getOpenTime())
		{
			tempoAtual += p.getLocTourist().getAvTime();
			if (tempoAtual < p.getLocTourist().getCloseTime())
				return true;
			else
				return false;
		}
		else
		{
			tempoEspera += p.getLocTourist().getOpenTime() - tempoAtual;
			tempoAtual += tempoEspera;
			tempoAtual += p.getLocTourist().getAvTime();
			return true;
		}
	}
	return false;
}

queue <stack <PLocTourist> > MapaDigital::viagemPersonalizada(PLocTourist lI, int tempoSaida, list<PLocTourist> lList, queue <int> &tComeco, queue <int> &tVisitas, queue <int> &tDeslocacao, queue <int> &mSaida, queue <int> &tEspera)
{

	queue < stack <PLocTourist> > caminhosDistintos;
	
	for each (PLocTourist x in lList)
	{
		queue <stack<PLocTourist>> caminhos = distinctPaths(lI, x);
		while (!caminhos.empty())
		{
			caminhosDistintos.push(caminhos.front());
			caminhos.pop();
		}
	}

	queue < stack <PLocTourist> > caminhosComTempo;

	while (!caminhosDistintos.empty()) {
		stack <PLocTourist> caminho = caminhosDistintos.front();
		if (caminho.size() != (lList.size() + 1))
			caminhosDistintos.pop();
		else
		{
			bool temLocNat = false;
			bool cumpreRequesitos = false;
			int tempoAtual = 0;
			int tempoComeco = 0;
			int tempoVisitas = 0;
			int tempoDeslocacao = 0;
			int tempoEspera = 0;

			stack<PLocTourist> reverse;
			while (!caminho.empty())
			{
				reverse.push(caminho.top());
				caminho.pop();
			}
			caminho = reverse;

			while (!caminho.empty())
			{
				cumpreRequesitos = false;
				if (!caminho.top().isLocHist())
					temLocNat = true;
				else
				{
					if (caminho.size() == (lList.size() + 1))
						tempoComeco = caminho.top().getLocTourist().getOpenTime();

					tempoVisitas += caminho.top().getLocTourist().getAvTime();
					/**/
					if (atualizaTempoAtualLocal(caminho.top(), tempoEspera, tempoAtual))
						cumpreRequesitos = true;

					if (tempoAtual < 1440 && tempoSaida >= tempoAtual)
					{
						for each (PLocTourist x in lList)
							if (caminho.top().getLocTourist().getID() == x.getLocTourist().getID() || lI.getLocTourist().getID() == x.getLocTourist().getID())
								cumpreRequesitos = true;

						if (caminho.top().getLocTourist().getID() == lI.getLocTourist().getID())
							cumpreRequesitos = true;
					}
					if (caminho.size() != 1)
					{
						stack<PLocTourist> mS = caminho;
						mS.pop();
						PPath p = getPath(caminho.top(), mS.top());
						tempoDeslocacao += p.getAvTime();
						tempoAtual += p.getAvTime();
					}
					/**/
				}
				caminho.pop();
			}

			if (tempoAtual < tempoSaida && !temLocNat && cumpreRequesitos && caminhosDistintos.front().size() == (lList.size() + 1))
			{
				tComeco.push(tempoComeco);
				mSaida.push(tempoAtual);
				tVisitas.push(tempoVisitas);
				tDeslocacao.push(tempoDeslocacao);
				tEspera.push(tempoEspera);
				caminhosComTempo.push(caminhosDistintos.front());
			}
			caminhosDistintos.pop();
		}
	}
	return caminhosComTempo;
}
#pragma endregion

#endif