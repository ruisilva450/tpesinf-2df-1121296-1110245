/***************************************
* HighWay.h
*
* Created on 9 de Outubro de 2013, 23:04
*
* @author Rui Silva & Daniela Grams
***************************************/

#ifndef HighWay_
#define HighWay_

#include <iostream>

using namespace std;

#include "Path.h"

#pragma region Class HighWay
/*classe responsavel pelas autoestradas*/
class HighWay:public Path
{
private:
	float tolls;
public:
	//default constructor
	HighWay();
	//constructor normal
	HighWay(string locTourist1, string locTourist2, string pName, int tKms, int avTrTime, float tolls);
	//constructor baseado num Path
	HighWay(const Path & p, float tolls);
	//constructor copia
	HighWay(const HighWay & hW);
	//destrutor
	~HighWay();

	//gets and sets
	float getTolls() const;
	void setTolls(float tolls);

	//methods
	void write(ostream & out) const;
	Path* clone() const;
	
};
#pragma endregion

#pragma region Construtores
//default constructor
HighWay::HighWay() :Path(), tolls(0)
{
}

//construtor normal
HighWay::HighWay(string locTourist1, string locTourist2, string pName, int tKms, int avTrTime, float tolls) : Path(locTourist1, locTourist2, pName, tKms, avTrTime)
{
	this->tolls = tolls;
}

//construtor baseado num Path
HighWay::HighWay(const Path & p, float tolls) : Path(p)
{
	this->tolls = tolls;
}

//construtor copia
HighWay::HighWay(const HighWay & hW) : tolls(hW.tolls), Path(hW)
{}

//destrutor
HighWay::~HighWay()
{}
#pragma endregion

#pragma region Gets e Sets
//sets and gets
float HighWay::getTolls() const
{
	return tolls;
}

void HighWay::setTolls(float tolls)
{
	this->tolls = tolls;
}
#pragma endregion

#pragma region Metodos
//apresenta a informacao sobre as auto estradas
void HighWay::write(ostream & out) const
{
	Path::write(out);
	out << "Tools Price: " << tolls << " euros" << endl;
}

ostream & operator << (ostream & out, const HighWay & p)
{
	p.write(out);
	return out;
}

//cria uma nova auto estrada a partir duma fornecida
Path* HighWay::clone() const
{
	return new HighWay(*this);
}
#pragma endregion

#endif