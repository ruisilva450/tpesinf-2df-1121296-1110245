#include <string>

using namespace std;

/*
MD/MDig- mapa digital
lt/locT/local-local turistico
locTH/locTHist- local turistico historico
locTN/locTNat- local turistico natural
path/vlig- vias de ligacao
vAuto- autoestrada
vNac- estrada nacional
*/

#pragma region Warnings
static string gcriado = "Grafo criado!";
static string charinv = "Caracter invalido, por favor tente novamente!!";
static string errImp = "Houve erros na importacao. Deseja ver o relatorio desses erros ? (ENTER para avancar, outra tecla para ver)";
static string nInfMDig = "O Mapa Digital nao contem nenhuma informacao ainda";
static string lTbyName = "Locais Turisticos ordenados pelo Nome!";
static string lTbyId = "Locais Turisticos ordenados pelo ID!";
static string not0area = "A area nao pode ser 0, tente novamente.";
static string invAreaNum = "Numero de area invalido! Tente novamente.";
static string verLocTHist = "!! Tem de ser um Local Turistico Historico !!";
static string sameLT = "!!Existe um Local Turistico com nome igual!!";
static string lessLocT = "!! Nao existem assim tantos Locais Turisticos !!";
static string locTHinsertError = "Houve erros na insercao do Local Turistico Historico. Deseja ver o relatorio desses erros? (Y ou outra letra)";
static string locTNinsertError = "Houve erros na insercao do Local Turistico Natural. Deseja ver o relatorio desses erros? (Y ou outra letra)";
static string pathInsertError = "Houve erros na insercao da Via de Ligacao. Deseja ver o relatorio desses erros? (Y ou outra letra)";
static string notEnoughTimeLeft = "O tempo restante do dia nao e o suficiente para o tempo medio de visita!";
static string notLocT = "!! Este Local Turistico nao existe !!";
static string cantCloseBeforeOpen = "!!Hora da saida nao pode ser antes da hora de entrada!!";
static string tooSmallTimeIntv = "O periodo em que o Local Turistico esta aberto nao e suficiente para uma visita com o tempo medio inserido";
static string not0openTime = "A hora de abertura nao pode ser 0. Tente novamente.";
static string notId0 = "Nao existe Locais Turisticos com a ID igual a 0.";
static string notIncLocal1 = "Nao pode incluir como Local \"";
static string notPassageLocal = "\" de passagem porque e ";
static string notIncLocal2 = "o Local Turistico de partida. (ignorado)";
static string notLocTHist = "um Local Turistico Historico (ignorado)";
static string tooManyLoc = "Seleccionou varias vezes o Local ";
static string charNotRecognized = "Introduziu caracteres nao reconhecidos. Tente novamente.";
static string notIdLoc = "Nao foi possivel escolher o local com a ID: ";
static string becauseIsnt = " porque nao e ";
static string notValidCancel = "Nao selecionou nenhum Local Turistico valido. Cancelado.";
static string notSameLoc = "O Local Turistico de origem e de destino nao podem ser os mesmos! Tente outro.";
static string not0closeTime = "A hora de saida nao pode ser 0, tente novamente.";
static string notEnoughLocT = "!!Nao existem Locais Turisticos suficientes para calcular um percurso!!";
static string notAgainlocTDest = "!! Nao pode escolher novamente o Local Turistico de destino !!";
static string notEnoughLTPaths = "!!Nao existem Locais Turisticos suficientes para fazer Vias de Ligacao!!";
static string invPathName = "Nome da estrada invalida!";
static string not1day = "Nao pode ter mais que 1 dia para fazer o percurso.";
static string sameDay = "O tempo de termino do percurso tem de ser no mesmo dia do inicio do percurso";
static string notLess1 = "O numero nao pode ser menor que 1, tente novamente.";
static string nInv = "Numero invalido, por favor tente novamente.";
static string dadosDel = "Dados Apagados!";
static string cpNenc = "Nao foram encontrados caminhos possiveis!";
#pragma endregion


#pragma region Menus
static string mp0 = "1 - Introduzir dados no Mapa Digital";
static string mp1 = "1 - Ver dados do Mapa Digital";
static string mp2 = "2 - Ver a complexidade do metodo de construcao do grafo";
static string mp3 = "3 - Ver todos os percursos possiveis entre dois Locais Turisticos";
static string mp4 = "4 - Calcular percurso entre dois locais Turisticos";
static string mp5 = "5 - Construir percurso";
static string mp9 = "9 - Outras opcoes";
static string sair = "0 - SAIR";

static string mAux1 = "1 - Introduzir dados automaticamente no Mapa Digital";
static string mAux2 = "2 - Introduzir manualmente dados no Mapa Digital";
static string mAux3 = "3 - Ver os nomes dos Locais Turisticos por ordem alfabetica";
static string mAux4 = "4 - Ver os Locais Turisticos";
static string mAux5 = "5 - Ver as Vias de Ligacao";
static string mAux6 = "Ver dados do Mapa Digital";
static string mAux7 = "7 - Ver dados do Grafo de Mapa Digital";
static string mAux8 = "8 - Ver a complexidade do metodo de construcao do grafo";
static string mAux9 = "9 - Ver todos os percursos possiveis entre dois Locais Turisticos";
static string mAuxA = "a - Calcular percurso entre dois Locais Turisticos";
static string mAuxB = "b - Construir percurso";
static string mAuxC = "c - Quanto tempo e necessario para percorrer todos os caminhos historicos";
static string mAuxD = "d - Ver percursos entre dois Locais Turisticos num periodo de tempo definido";
static string mAuxE = "e - Ver percurso completo a partir de um Local de partida";

static string delDados = "Apagar Dados";
static string mPrinc = "0 - Voltar ao Menu Principal";

static string man1 = "1 - Inserir Local Turistico Natural";
static string man2 = "2 - Inserir Local Turistico Historico";
static string man3 = "3 - Inserir Via de Ligacao";

static string cp1 = "1 - Mais curto em Kms";
static string cp2 = "2 - Mais economico em Euros";
static string cp3 = "3 - Maior interesse turistico";
static string mAnterior = "0 - Voltar ao menu anterior";

static string sortM1 = "1 - Ordenar os locais turisticos pelo nome internamente";
static string sortM2 = "2 - Ordenar os locais turisticos pelo ID internamente";
static string sortM0 = "0 - Sair sem ordenar internamente";
#pragma endregion


#pragma region ListarInfo
static string tnecs = "O tempo necessario para visitar todos os ";
static string lIntT = "Locais de Interesse Turisticos e : ";
static string op = "Introduza a opcao: ";
static string locHist = "(Local Historico)\t";
static string locNat = "(Local Natural)\t\t";
static string mDig = "Mapa Digital com: ";
static string lTur = " Locais Turisticos";
static string lHist = " Lugares Historicos";
static string lNat = " Lugares Naturais";
static string vLig = " Vias de Ligacao";
static string vAuto = " Autoestradas";
static string vNac = " Estradas Nacionais";
static string listLT = "Locais Turisticos: ";
static string locTHName = "Nome do Local Turistico Historico: ";
static string locTNName = "Nome do Local Turistico Natural: ";
static string areaTN = "Area: ";
static string squareMeters = " metros quadrados";
static string avgVisitTime = "Tempo medio de visita: ";
static string openTime = "Hora de abertura: ";
static string closeTime = "Hora de fecho: ";
static string locTOriginName = "Nome do Local Turistico de partida: ";
static string locTDestName = "Nome do Local Turistico de destino: ";
static string pathName = "Nome da estrada percorrida: ";
static string totKm = "Numero total de Kilometros: ";
static string avgTravelTime = "Tempo medio de viagem: ";
static string km = " kms ";
static string eur = " euro";
static string totToll = "Total das portagens: ";
static string pavType = "Tipo de pavimento: ";
static string pathEndTime = "Tempo do fim do percurso: ";
static string locTIn = "Local Turistico inicial: ";
static string locTFim = "Local Turistico Final: ";
static string timeAvailable = "Tempo disponivel para fazer o percurso: ";
static string pathsBetween = "Caminhos entre ";
static string possiblePaths = "Caminhos possiveis: ";
static string andPath = "Caminhos: ";
static string inTime = "No tempo: ";
static string pathDetails = "Detalhes do percurso: ";
static string iniTime = "Hora de inicio do percurso: ";
static string visitTime = "Tempo em visitas: ";
static string exitLocTime = "Hora de saida do ultimo Local Turistico: ";
static string acWaitTime = "Tempo de espera acumulado: ";
static string acPathTime = "Tempo em deslocacao: ";
static string fullPathFrom = "Percurso completo a partir do Local: \"";
static string chosenPassLoc = "Locais Turisticos de passagem escolhidos: ";
static string cCompGrafo = "\tComplexidade do metodo de construcao do grafo\n\t\t\t\t";
static string cGSignificado = "\tSignifica que a complexidade e proporcional ao numero de\n\t\tLocais Turisticos x Vias de Ligacao ";
static string selectLocTInstruction = "Para escolher escreva os numeros relativos as IDs separados por virgula.\nPor exemplo: 2,5,6,8 (ENTER para cancelar)\nEscolha os Locais Turisticos Historicos que pretende passar no seu percurso turistico:\n";
static string LocIn = "\t(Local Inicial)";
#pragma endregion


#pragma region getInfo
static string indiqIdHOrig = "Indique a ID do Local Turistico Historico de partida? (ENTER para cancelar)";
static string indiqIdHDestino = "Indique a ID do Local Turistico Historico de destino? (ENTER para cancelar)";
static string indiqIdOrig = "Indique a ID do Local Turistico de partida? (ENTER para cancelar)";
static string indiqIdDest = "Indique a ID do Local Turistico de destino? (ENTER para cancelar)";
static string indiqNomeLTOrig = "Qual o nome do Local Turistico de partida? ([ENTER] para abortar)";
static string indiqNomeLTDest = "Qual o nome do Local Turistico de destino? ([ENTER] para abortar)";
static string indiqNomeLH = "Qual o nome do Local Turistico Historico? ([ENTER] para abortar)";
static string indiqNomeLN = "Qual o nome do Local Turistico Natural? ([ENTER] para abortar)";
static string indiqAreaLN = "Qual a area do Local Turistico Natural ? ([ENTER] para abortar)";
static string indiqCloseTime = "Qual a hora de fecho? ([ENTER] para abortar)";
static string indiqOpenTime = "Qual a hora de abertura? ([ENTER] para abortar)";
static string indiqAvgVisitTime = "Qual o tempo medio de visita? ([ENTER] para abortar)";
static string indiqTotTolls = "Qual o total das portagens? ([ENTER] para abortar)";
static string indiqAvgTime = "Qual o tempo medio de viagem? ([ENTER] para abortar)";
static string indiqPavType = "Qual o tipo de pavimento? ([ENTER] para abortar)";
static string indiqNomePath ="Qual o nome da estrada percorrida? ([ENTER] para abortar)";
static string howLong = "Quando pretende terminar o percurso? (0 para cancelar)";
static string indiqTotKm = "Qual o numero total de Kms? ([ENTER] para abortar)";
static string checkInfo = "Estas informacoes estao correctas? (Y ou outra letra)";
#pragma endregion


//metodo para verificar o horario de funcionamento dos locais turisticos
static string timeConverter(int i)
{
	if (i > 1440)
		return "!!Nao pode ultrapassar as 24 horas!!";
	else
	{
		stringstream o;
		int hours = -1, minutes = -1, time = 0;
		hours = i / 60;
		if (hours != 0)
		{
			o << hours << " hora";
			if (hours != 1)
				o << "s";
		}

		minutes = i % 60;
		if (hours != 0 && minutes != 0)
			o << " e ";
		if (minutes != 0)
		{
			o << minutes << " minuto";
			if (minutes != 1)
				o << "s";
		}

		return o.str();
	}
}

#pragma region Trimmers
// trim from start
static inline string &ltrim(string &s) {
	s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
	return s;
}
// trim from end
static inline string &rtrim(string &s) {
	s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());
	return s;
}
// trim from both ends
static inline string &trim(string &s) {
	return ltrim(rtrim(s));
}
#pragma endregion