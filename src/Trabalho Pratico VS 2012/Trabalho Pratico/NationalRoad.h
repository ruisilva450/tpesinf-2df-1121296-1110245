/***************************************
* NationalRoad.h
*
* Created on 9 de Outubro de 2013, 23:12
*
* @author Rui Silva & Daniela Grams
***************************************/

#ifndef NationalRoad_
#define NationalRoad_

#include <iostream>
#include "Path.h"

#pragma region Class NationalRoad
/*classe responsavel pelas estradas nacionais*/
class NationalRoad :public Path
{
private:
	//variavel especifica da estrada nacional
	string pavType;
public:
	//default constructor
	NationalRoad();
	//constructor normal
	NationalRoad(string locTourist1, string locTourist2, string pName, int tKms, int avTrTime, string pavType);
	//constructor baseado num Path
	NationalRoad(const Path & p, string pavType);
	//constructor copia
	NationalRoad(const NationalRoad & hW);
	//destrutor
	~NationalRoad();

	//gets and sets
	string getPavType() const;
	void setPavType(string pavType);

	//methods
	void write(ostream & out) const;
	Path* clone() const;
};
#pragma endregion

#pragma region Construtores
//construtor default
NationalRoad::NationalRoad() :Path()
{
	pavType = "";
}

//construtor normal
NationalRoad::NationalRoad(string locTourist1, string locTourist2, string pName, int tKms, int avTrTime, string pavType) 
: Path(locTourist1, locTourist2, pName, tKms, avTrTime)
{
	this->pavType = pavType;
}

//construtor baseado num Path
NationalRoad::NationalRoad(const Path & p, string pavType) : Path(p)
{
	this->pavType = pavType;
}

//construtor copia
NationalRoad::NationalRoad(const NationalRoad & hW) : pavType(hW.pavType), Path(hW)
{}

//destrutor
NationalRoad::~NationalRoad()
{}
#pragma endregion

#pragma region Gets e Sets
//sets and gets
string NationalRoad::getPavType() const
{
	return pavType;
}

void NationalRoad::setPavType(string pavType)
{
	this->pavType = pavType;
}
#pragma endregion

#pragma region Metodos
//guarda a informacao da estrada como string
void NationalRoad::write(ostream & out) const
{
	Path::write(out);
	out << "Type of Trackway: " << pavType << endl;
}

ostream & operator << (ostream & out, const NationalRoad & p)
{
	return out;
}

//copia a informacao de uma estrada nacional para outra
Path* NationalRoad::clone() const
{
	return new NationalRoad(*this);
}
#pragma endregion

#endif