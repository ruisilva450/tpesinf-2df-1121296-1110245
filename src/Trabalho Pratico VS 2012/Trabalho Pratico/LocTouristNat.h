/***************************************
* LocTouristNat.h
*
* Created on 8 de Outubro de 2013, 14:10
*
* @author Rui Silva & Daniela Grams
***************************************/

#ifndef LocTouristNat_
#define LocTouristNat_

#include <stdio.h>
#include <iostream>
#include "LocTourist.h"

using namespace std;

#pragma region Classe LocTouristNat
/*classe responsavel pelos locais turisticos naturais*/
class LocTouristNat: public LocTourist
{
private:
//variaveis espicificas dos locais turisticos naturais
	int area;
public:
	//constructor padrao
	LocTouristNat();

	//constructor normal
	LocTouristNat(string n, int a, int id=0);

	//constructor copia
	LocTouristNat(const LocTouristNat & lt);

	//destructor
	~LocTouristNat();

	//gets and sets
	int getArea() const;
	void setArea(int a);

	//methods
	void write(ostream & out) const;
	LocTourist* clone() const;
};
#pragma endregion

#pragma region Construtores
//constructor padrao
LocTouristNat::LocTouristNat() :LocTourist()
{
	area = -1;
}

//constructor normal
LocTouristNat::LocTouristNat(string n, int a, int id) :LocTourist(id, n)
{
	area = a;
}

//constructor copia
LocTouristNat::LocTouristNat(const LocTouristNat & ln)
{
	area = ln.area;
}

//destructor
LocTouristNat::~LocTouristNat(){}
#pragma endregion

#pragma region Gets e Sets
//gets and sets
void LocTouristNat::setArea(int a)
{
	area=a;
}

int LocTouristNat::getArea() const 
{
	return area;
}
#pragma endregion

#pragma region Metodos
//methods
//guarda a informacao de um local natural em string
void LocTouristNat::write(ostream& out) const{
	LocTourist::write(out);
	out << "Area: " << area << " metros quadrados" << endl;
}

//copia a informacao de um local natural para outro
LocTourist* LocTouristNat::clone() const{
	return new LocTouristNat(*this);
}
#pragma endregion

#endif