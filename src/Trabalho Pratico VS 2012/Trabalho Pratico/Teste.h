/***************************************
* Teste.h
*
* Created on 21 de Outubro de 2013, 00:01
*
* @author Rui Silva & Daniela Grams
***************************************/

#ifndef Teste_
#define Teste_

#define MAX_LOCTOURIST 256		//maximo de locais turisticos

#include <iostream>
#include <fstream>
#include <vector>
#include <deque>
#include <list>

using namespace std;

#include "LocTourist.h"
#include "LocTouristNat.h"
#include "LocTouristHist.h"
#include "PLocTourist.h"

#include "Path.h"
#include "HighWay.h"
#include "NationalRoad.h"
#include "PPath.h"

#include "MapaDigital.h"

#pragma region Classe Teste
/*Esta classe � respons�vel pela importa��o da informa��o dos ficheiros.
* A informa��o � guardada em contentores e h� m�todos de manipula��o*/
class Teste
{
private:
	deque<PLocTourist> locTourists;
	vector<PPath> vecPPath;

	//methods
	string splitFilename(const string& str);
	void readFile(fstream & f, const string & fName, stringstream &resume, int fileId = 0);

public:
	//constructor padrao
	Teste();
	//constructor normal
	Teste(const Teste & t);
	//destructor
	~Teste();

	//sets and gets
	deque <PLocTourist> getLocTourists();
	vector <PPath> getVecPaths();

	//information insertion
	bool insertLocTourist(string data[], int i, stringstream &resume);
	bool insertPath(string data[], stringstream &resume);

	//validations
	bool isTesteEmpty() const;
	bool isPath(string locTourist1Name, string locTourist2Name) const;
	PLocTourist getLocTourist(string locT) const;
	PLocTourist getLocTourist(int iD) const;
	PPath getPath(string o, string d) const;
	PPath getPath(PLocTourist o, PLocTourist d) const;
	bool isLocTourist(string locT) const;
	bool isLocTouristDependent(string locT) const;

	bool isHighWayValid(string locTourist1, string locTourist2, string pName, int tKms, int avTrTime, float tolls) const;
	bool isNationalRoadValid(string locTourist1, string locTourist2, string pName, int tKms, int avTrTime, string pavType) const;
	bool isLocTouristNatValid(string n, int a) const;
	bool isLocTouristHistValid(string n, int avT, int opT, int clT) const;

	//methods
	int getNumLocTouristHist();
	int getNumLocTouristNat();
	int getNumHighWays();
	int getNumNationalRoads();
	vector<string> locTouristNames();
	stringstream readFiles(const deque<string> files);
	int locTouristsSize();
	int vecPathsSize();
	bool islocTouristsEmpty();
	bool isvecPathEmpty();

	void sortLocTouristsByID();
	void sortLocTouristsByName();

	void clear();

	void escreve(ostream & out) const;
};
#pragma endregion

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\PRIVATE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#pragma region Metodos
//////////////////////////////////////METHODS//////////////////////////////////////////

//obtem o nome do ficheiro a partir da string inserida
string Teste::splitFilename(const string& str)
{
	unsigned found = str.find_last_of("/\\");
	return str.substr(found + 1);
}

//le e transforma a informacao encontrada nas linhas dum ficheiro para o objecto correspondente
//o parametro fileId pode tomar os seguintes valores:
//	fileId == 0 : A informacao est� toda contida em 1 s� ficheiro (Locais Turisticos & Vias de Liga��o)
//	fileId == 1 : O ficheiro cont�m informa��o relativa a Locais Turisticos
//	fileId == 2 : O ficheiro cont�m informa��o relativa a Vias de Liga��o
void Teste::readFile(fstream& file, const string& filename, stringstream &resume, int fileId)
{
	int i = 0;
	cout << "Reading data from file \"" << splitFilename(filename) << "\"" << endl;

	file.open(filename, ios::in); //Abre o ficheiro

	if (file.is_open()) //Se conseguir abrir o ficheiro...
	{
		stringstream iss;
		string line, temp;
		string data[6];
		int lineNumber=0;
		while (getline(file, line))	// Contem a linha completa at� ao '\n'
		{
			lineNumber++;
			iss << line;
			while (getline(iss, temp, ',')) //Guarda a informa��o na variavel temp baseado no que a variavel iss cont�m mas separado por ','
			{
				if (temp != "" && i < 6)
				{
					data[i] = temp;
					i++;
				}
				else
				{
					if (temp != "")
						resume << "!Informacao incompativel na linha : " << lineNumber << "!" << endl;
				}
			}
			// **Fim da linha**
			bool flag = true;
			for (int k = 0; k < i; k++)
			{
				if (data[k] == "") flag = false;
			}
			if (i != 0)
			{
				if (flag)
				{
					if (fileId == 0)
					{
						if (i == 4 || i == 2)
							insertLocTourist(data, i, resume);
						else
						if (i == 6)
						{
							if (resume.str() == "")
								insertPath(data, resume);
							else
							{
								if (resume.str().at(0) == '!' && vecPPath.size() == 0)
								{
									resume << "---------------------------------------------------------------" << endl;
									resume << "Normalmente quando alguns Locais Turisticos nao sao importados,";
									resume << " as Vias de Ligacao pendentes desses Locais nao vao ser inseridos!" << endl;
									resume << "---------------------------------------------------------------" << endl;
									insertPath(data, resume);
								}
							}						
						}
						else
							resume << "!Informacao incompativel na linha : " << lineNumber << "!" << endl;
					}
					if (fileId == 1) //./files/Locais Turisticos.txt"
					if (insertLocTourist(data, i, resume))
						resume << "!Informacao incompativel na linha : " << lineNumber << "!" << endl;
					if (fileId == 2) //./files/Vias de Ligacao.txt
					if (insertPath(data, resume))
						resume << "!Informacao incompativel na linha : " << lineNumber << "!" << endl;
				}
				else
					resume << "!Ficheiro com conteudo incompativel!" << endl;
			}
			for (int j = 0; j < i; j++) //Limpa a informa��o contida na variavel data
			{
				data[j] = "";
			}
			i = 0;
			iss.clear();
		}
	}
	file.close(); //close the file
}
#pragma endregion

#pragma region Validacoes
//////////////////////////////////////VALIDATIONS//////////////////////////////////////////

//devolve verdadeiro se a auto estrada tem informacao valida
bool Teste::isHighWayValid(string locTourist1, string locTourist2, string pName, int tKms, int avTrTime, float tolls) const
{
	return (locTourist1.compare("") && locTourist2.compare("") && pName.compare("") && tKms != 0 && avTrTime != 0 && tolls != 0);
}

//devolve verdadeiro se a estrada nacional tem informacao valida
bool Teste::isNationalRoadValid(string locTourist1, string locTourist2, string pName, int tKms, int avTrTime, string pavType) const
{
	return (locTourist1.compare("") && locTourist2.compare("") && pName.compare("") && tKms != 0 && avTrTime != 0 && pavType.compare(""));
}

//devolve verdadeiro se o local natural tem informacao valida
bool Teste::isLocTouristNatValid(string n, int a) const
{
	return (n.compare("") && a != 0);
}

//devolve verdadeiro se o local historico tem informacao valida
bool Teste::isLocTouristHistValid(string n, int avT, int opT, int clT) const
{
	return (n.compare("") && avT != 0 && opT != 0 && clT != 0);
}

//verifica se  nao ha informacao introduzida
bool Teste::isTesteEmpty() const
{
	return(vecPPath.empty() && locTourists.empty());
}

//devolve verdadeiro se uma Path com o mesmo nome ja existe
bool Teste::isPath(string locTourist1Name, string locTourist2Name) const
{
	for (unsigned i = 0; i < vecPPath.size(); i++)
	{
		string locT1 = vecPPath.at(i).getPath().getlocTourist1();
		string locT2 = vecPPath.at(i).getPath().getlocTourist2();
		if (vecPPath.at(i).getPath().getlocTourist1() == locTourist1Name && vecPPath.at(i).getPath().getlocTourist2() == locTourist2Name)
		{
			return true;
		}
	}
	return false;
}

//devolve o objecto PLocTourist se um LocTourist com o mesmo nome ja existe
PLocTourist Teste::getLocTourist(string locT) const 
{
	if (isLocTourist(locT))
	{
		deque <PLocTourist> auxQueue = locTourists;
		PLocTourist aux;
		while (!auxQueue.empty())
		{
			for (unsigned i = 0; i < auxQueue.size(); i++)
			{
				aux = auxQueue.front();
				if (aux.getLocTourist().getName() == locT)
				{
					return aux;
				}
				auxQueue.pop_front();
			}
		}
	}
	PLocTourist l;
	return l;
}

PLocTourist Teste::getLocTourist(int iD) const
{
	deque <PLocTourist> auxQueue = locTourists;
	PLocTourist aux;
	while (!auxQueue.empty())
	{
		for (unsigned i = 0; i < auxQueue.size(); i++)
		{
			aux = auxQueue.front();
			if (aux.getLocTourist().getID() == iD)
				return aux;
			auxQueue.pop_front();
		}
	}
	PLocTourist l;
	return l;
}

PPath Teste::getPath(string o, string d) const
{
	vector<PPath> auxVector = vecPPath;
	PPath aux;
	while (!auxVector.empty())
	{
		for (unsigned i = 0; i < auxVector.size(); i++)
		{
			aux = auxVector.back();
			if (aux.getPath().getlocTourist1() == o && aux.getPath().getlocTourist2() == d)
				return aux;
			auxVector.pop_back();
		}
	}
	PPath p;
	return p;
}

PPath Teste::getPath(PLocTourist o, PLocTourist d) const
{
	vector<PPath> auxVector = vecPPath;
	PPath aux;
	while (!auxVector.empty())
	{
		for (unsigned i = 0; i < auxVector.size(); i++)
		{
			aux = auxVector.back();
			if (getLocTourist(aux.getPath().getlocTourist1()) == o && getLocTourist(aux.getPath().getlocTourist2()) == d)
				return aux;
			auxVector.pop_back();
		}
	}
	PPath p;
	return p;
}

bool Teste::isLocTourist(string locT) const
{
	deque <PLocTourist> auxQueue = locTourists;
	PLocTourist aux;
	while (!auxQueue.empty())
	{
		for (unsigned i = 0; i < auxQueue.size(); i++)
		{
			aux = auxQueue.front();
			if (aux.getLocTourist().getName() == locT)
			{
				return true;
			}
			auxQueue.pop_front();
		}
	}
	return false;
}

//devolve verdadeiro se o LocTourist com o nome dado depende de Paths e nao pode ser eliminado (se devolver verdadeiro nao e eliminado)
bool Teste::isLocTouristDependent(string locT) const
{
	for (unsigned i = 0; i < vecPPath.size(); i++)
	{
		if (vecPPath.at(i).getPath().getlocTourist1() == locT && vecPPath.at(i).getPath().getlocTourist2() == locT)
		{
			return true;
		}
	}
	return false;
}
#pragma endregion
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\PUBLIC\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#pragma region Construtores
//construtor default
Teste::Teste() : locTourists(), vecPPath()
{
}

//construtor copia
Teste::Teste(const Teste & t) : locTourists(t.locTourists), vecPPath(t.vecPPath)
{
}

//destrutor
Teste::~Teste()
{
	//para apagar
	locTourists.clear();
	vecPPath.clear();
}
#pragma endregion

#pragma region Gets e Sets
//devolve uma deque de apontadores para os Locais Turisticos guardados
deque <PLocTourist> Teste::getLocTourists()
{
	return this->locTourists;
}

//devolve um vector de apontadores para as Vias de Liga��o guardadas
vector <PPath> Teste::getVecPaths()
{
	return this->vecPPath;
}
#pragma endregion

#pragma region Inserir Informacao
//Insere um Local Turistico
bool Teste::insertLocTourist(string data[], int i, stringstream & resume)
{
	if (locTourists.size() >= MAX_LOCTOURIST)
	{
		resume << "Espa�o insuficiente para guardar mais Locais Turisticos!" << endl;
		return false; 
	}
	string locTouristName = data[0].c_str();
	if (isLocTourist(locTouristName))
	{
		resume << "Local Turistico com nome \"" << locTouristName << "\" ja criado!" << endl;
		return false;
	}
	else
	{
		if (i == 4)
		{
			//historicos
			int locTouristHistTempMedio = atoi(data[1].c_str());
			int locTouristHistHoraEntrada = atoi(data[2].c_str());
			int locTouristHistHoraSaida = atoi(data[3].c_str());

			//validacao da informacao
			if (isLocTouristHistValid(locTouristName, locTouristHistTempMedio, locTouristHistHoraEntrada, locTouristHistHoraSaida))
			{
				LocTourist* ltH = new LocTouristHist(locTouristName, locTouristHistTempMedio,
					locTouristHistHoraEntrada, locTouristHistHoraSaida);
				ltH->setID(locTourists.size() + 1);
				PLocTourist lT;
				lT.setLocTourist(ltH);
				locTourists.push_back(lT);
				return true;
			}
		}
		if (i == 2)
		{
			//naturais
			int locTouristNatArea = atoi(data[1].c_str());

			//validacao da informacao
			if (isLocTouristNatValid(locTouristName, locTouristNatArea))
			{
				LocTourist* ltN = new LocTouristNat(locTouristName, locTouristNatArea);
				ltN->setID(locTourists.size() + 1);
				PLocTourist lT;
				lT.setLocTourist(ltN);
				locTourists.push_back(lT);
				return true;
			}
		}
	}
	return false;
}

//Insere uma Via
bool Teste::insertPath(string data[], stringstream &resume)
{
	string LocTourist1Name = data[0].c_str();
	string LocTourist2Name = data[1].c_str();
	string pathName = data[2].c_str();
	int tTotalKms = atoi(data[3].c_str());
	int tempMed = atoi(data[4].c_str());

	if (isPath(LocTourist1Name, LocTourist2Name))
	{
		resume << "-------------------------------------------------------------------------------" << endl;
		resume << "Via de Ligacao entre \"" << LocTourist1Name << "\" e \"" << LocTourist2Name << "\" ja criada!" << endl;
		resume << "-------------------------------------------------------------------------------" << endl;
	}
	else
	{
		if (pathName.at(0) == 'A') //verifies if it is a HighWay name designation
		{
			float tolls = (float)atof(data[5].c_str());
			if (isHighWayValid(LocTourist1Name, LocTourist2Name, pathName, tTotalKms, tempMed, tolls) && isLocTourist(LocTourist1Name) && isLocTourist(LocTourist2Name))
			{
				Path * hW = new HighWay(LocTourist1Name, LocTourist2Name, pathName, tTotalKms, tempMed, tolls);
				PPath p;
				p.setPath(hW);
				vecPPath.push_back(p);
				return true;
			}
			else
			{
				resume << "--------------------------------------------------------------------------------";
				resume << "Impossivel criar um caminho entre \"" << LocTourist1Name << "\" e \"" << LocTourist2Name << "\" na estrada \"" << pathName << "\"" << endl;
				if (!isLocTourist(LocTourist1Name))
				{
					if (!isLocTourist(LocTourist2Name))
					{
						resume << "Porque \"" << LocTourist1Name << "\" e \"" << LocTourist2Name << "\" nao existem!" << endl;
					}
					else
					{
						resume << "Porque \"" << LocTourist1Name << "\" nao existe!" << endl;
					}
				}
				else
				{
					if (!isLocTourist(LocTourist2Name))
					{
						resume << "Porque \"" << LocTourist2Name << "\" nao existe!" << endl;
					}
				}
				resume << "--------------------------------------------------------------------------------";
				return  false;
			}
		}
		if (pathName.at(0) == 'E') //verifies if it is a National Road name designation
		{
			string pavType = data[5].c_str();
			if (isNationalRoadValid(LocTourist1Name, LocTourist2Name, pathName, tTotalKms, tempMed, pavType) && isLocTourist(LocTourist1Name) && isLocTourist(LocTourist2Name))
			{
				Path * nR = new NationalRoad(LocTourist1Name, LocTourist2Name, pathName, tTotalKms, tempMed, pavType);
				PPath p;
				p.setPath(nR);
				vecPPath.push_back(p);
				return true;
			}
			else
			{
				resume << "--------------------------------------------------------------------------------";
				resume << "Impossivel criar um caminho entre \"" << LocTourist1Name << "\" e \"" << LocTourist2Name << "\" na estrada \"" << pathName << "\"" << endl;
				if (!isLocTourist(LocTourist1Name))
				{
					if (!isLocTourist(LocTourist2Name))
					{
						resume << "Porque \"" << LocTourist1Name << "\" e \"" << LocTourist2Name << "\" nao existem!" << endl;
					}
					else
					{
						resume << "Porque \"" << LocTourist1Name << "\" nao existe!" << endl;
					}
				}
				else
				{
					if (!isLocTourist(LocTourist2Name))
					{
						resume << "Porque \"" << LocTourist2Name << "\" nao existe!" << endl;
					}
				}
				resume << "--------------------------------------------------------------------------------";
				return false;
			}
		}
	}
	return false;
}
#pragma endregion

#pragma region Metodos
int Teste::getNumLocTouristHist()
{
	int n = 0;
	for (deque<PLocTourist>::iterator ite = locTourists.begin(); ite != locTourists.end(); ite++)
	{
		if (typeid(ite->getLocTourist()) == typeid(LocTouristHist))
		{
			n++;
		}
	}
	return n;
}

int Teste::getNumLocTouristNat()
{
	int n = 0;
	for (deque<PLocTourist>::iterator ite = locTourists.begin(); ite != locTourists.end(); ite++)
	{
		if (typeid(ite->getLocTourist()) == typeid(LocTouristNat))
		{
			n++;
		}
	}
	return n;
}

int Teste::getNumHighWays()
{
	int n = 0;
	for (vector<PPath>::iterator ite = vecPPath.begin(); ite != vecPPath.end(); ite++)
	{
		if (typeid(ite->getPath()) == typeid(HighWay))
		{
			n++;
		}
	}
	return n;
}

int Teste::getNumNationalRoads()
{
	int n = 0;
	for (vector<PPath>::iterator ite = vecPPath.begin(); ite != vecPPath.end(); ite++)
	{
		if (typeid(ite->getPath()) == typeid(NationalRoad))
		{
			n++;
		}
	}
	return n;
}

//apresenta os nomes dos locais turisticos por ordem alfabetica
vector<string> Teste::locTouristNames()
{
	vector <string> s;
	deque<PLocTourist> auxQueue = locTourists;
	PLocTourist aux;
	cout << endl << "Por ordem alfabetica de nome dos locais turisticos:" << endl;
	while (!auxQueue.empty())
	{
		for (unsigned i = 0; i < auxQueue.size(); i++)
		{
			aux = auxQueue.front();
			s.push_back(aux.getLocTourist().getName());
			auxQueue.pop_front();
		}
	}

	sort(s.begin(), s.end());
	return s;
}

//Le os ficheiros 
stringstream Teste::readFiles(const deque<string> files)
{
	stringstream resume;
	fstream file;
	if (files.size() == 2)
	{
		#pragma region Ler informa��o em 2 ficheiros:
		cout << "________________________________________________" << endl;
		cout << "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv" << endl;
		readFile(file, files[0], resume, 1); //Locais Turisticos
		if ((locTourists.size()) == 0)
			cout << "Unable to import data from file \"" << splitFilename(files[0]) << "\"" << endl;
		cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << endl;
		cout << "________________________________________________" << endl;
		cout << "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv" << endl;
		readFile(file, files[1], resume, 2); //Vias de Ligacao
		if ((vecPPath.size()) == 0)
			cout << "Unable to import data from file \"" << splitFilename(files[1]) << "\"" << endl;
		cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << endl;
		#pragma endregion
		return resume;
	}
	if (files.size() == 1)
	{
		#pragma region Ler informacao em 1 ficheiro:
		cout << "________________________________________________" << endl;
		cout << "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv" << endl;
		readFile(file, files[0], resume);
		cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << endl;
		cout << "________________________________________________" << endl;
		#pragma endregion
		return resume;
	}
	resume << "Erro ao ler os ficheiros!";
	return resume; 
}

//tamanho da deque locTourists
int Teste::locTouristsSize()
{
	return (int)locTourists.size();
}

//tamanho do vecot vecPath
int Teste::vecPathsSize()
{
	return (int)vecPPath.size();
}

//ve se a deque locTourists esta vazia
bool Teste::islocTouristsEmpty()
{
	return locTourists.empty();
}

//ve se o vector vecPath esta vazia
bool Teste::isvecPathEmpty()
{
	return vecPPath.empty();
}

//compara o nome de dois locais turisticos alfabeticamente
bool compareLocTouristsName(PLocTourist o1, PLocTourist o2)
{
	return (o1.getLocTourist().getName() < o2.getLocTourist().getName());
}

//compara o ID de dois locais turisticos
bool compareLocTouristsID(PLocTourist o1, PLocTourist o2)
{
	return (o1.getLocTourist().getID() < o2.getLocTourist().getID());
}

//ordena a deque locTourists por ID dos locais turisticos
void Teste::sortLocTouristsByID()
{
	sort(locTourists.begin(), locTourists.end(), compareLocTouristsID);
}

//ordena a deque locTourist por nome dos locais turisticos
void Teste::sortLocTouristsByName()
{
	sort(locTourists.begin(), locTourists.end(), compareLocTouristsName);
}

//limpa a deque locTourists e o vector vecPath
void Teste::clear()
{
	locTourists.clear();
	vecPPath.clear();
}

//guarda a informacao do mapa, locais e vias em formato String
void Teste::escreve(ostream & out) const
{
	if (!isTesteEmpty())
	{
		//info about Mapa Digital
		out << "Mapa Digital com: " << locTourists.size() << " Locais Turisticos" << endl;
		out << "e " << vecPPath.size() << " caminhos" << endl;

		//List of Tourist Places
		out << "Tourist Places: " << endl;
		for each (PLocTourist x in locTourists)
		{
			out << x.getLocTourist() << endl;
		}

		//List of vecPaths
		out << "Caminhos: " << endl;
		for each (PPath x in vecPPath)
		{
			out << x.getPath() << endl;
		}
	}
	else
	{
		out << "O Mapa Digital nao contem nenhuma informacao ainda";
	}
}
#pragma endregion

#pragma region Operadores
//escreve a informacao do mapa, locais e vias previamente guardada em formato string
ostream & operator << (ostream& out, Teste& mD)
{
	mD.escreve(out);
	return out;
}
#pragma endregion

#endif