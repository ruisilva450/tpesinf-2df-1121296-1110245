#ifndef PPath_
#define PPath_

#include "Path.h"
#include "HighWay.h"
#include "NationalRoad.h"

#pragma region classe PPath
class PPath
{
private:
	Path * path;

	enum TipoComparacao {KMS, TOLLS, AVTIME};
	static TipoComparacao tipoComparacao;
public:
	PPath();
	PPath(int tKms, int avTrTime, float tolls);
	PPath(const Path & p);
	PPath(const PPath & p);
	~PPath();

	//gets and sets
	static void setComparacaoKMS();
	static void setComparacaoCUSTO();
	static void setComparacaoAVTIME();
	void setPath(Path* p);
	Path & getPath() const;
	int getKm() const;
	void setKm(int n);
	int getAvTime() const;
	void setAvTime(int n);
	float getTolls() const;
	void setTolls(float n);

	//operators
	bool operator>(PPath & p) const;
	bool operator<(PPath & p) const;
	bool operator == (const PPath & p) const;
	PPath & operator+(const PPath & p);
	const PPath & operator+=(const PPath & p);
	const PPath & operator= (const PPath & p);
	void write(ostream & out) const;
	PPath* clone() const;
};
#pragma endregion

PPath::TipoComparacao PPath::tipoComparacao=PPath::TipoComparacao::KMS;

#pragma region Constructores

PPath::PPath()
{
	this->path = new HighWay();
}

PPath::PPath(int tKms, int avTrTime, float tolls)
{
	path = new HighWay("", "", "", tKms, avTrTime, tolls);
}

PPath::PPath(const Path & p)
{
	this->path = p.clone();
}

PPath::PPath(const PPath & p)
{
	path = p.path->clone();
}

PPath::~PPath()
{
}

#pragma endregion

#pragma region Gets e Sets
void PPath::setComparacaoKMS() {
	tipoComparacao=TipoComparacao::KMS;
}
void PPath::setComparacaoCUSTO() {
	tipoComparacao=TipoComparacao::TOLLS;
}
void PPath::setComparacaoAVTIME() {
	tipoComparacao=TipoComparacao::AVTIME;
}

void PPath::setPath(Path * p)
{
	path = p;
}

Path & PPath::getPath() const
{
	return *path;
}

int PPath::getKm() const
{
	return path->getTotalKms();
}

void PPath::setKm(int n)
{
	this->path->setTotalKms(n);
}

int PPath::getAvTime() const
{
	return path->getAvTravelTime();
}

void PPath::setAvTime(int n)
{
	this->path->setAvTravelTime(n);
}

float PPath::getTolls() const
{
	if (typeid(*path) == typeid(HighWay))
		return dynamic_cast<HighWay *>(path)->getTolls();
	return 0;
}

void PPath::setTolls(float n)
{
	if (typeid(*path) == typeid(HighWay))
		dynamic_cast<HighWay *>(path)->setTolls(n);
}
#pragma endregion

#pragma region Operadores
bool PPath::operator>(PPath & p) const
{
	if (tipoComparacao == TipoComparacao::AVTIME) 
		return this->path->getAvTravelTime() > p.path->getAvTravelTime();
	if (tipoComparacao == TipoComparacao::TOLLS) 
		return this->path->getTolls() > p.path->getTolls();
	return this->path->getTotalKms() > p.path->getTotalKms();
}

bool PPath::operator<(PPath & p) const
{
	if (tipoComparacao == TipoComparacao::AVTIME)
		return this->path->getAvTravelTime() < p.path->getAvTravelTime();
	if (tipoComparacao == TipoComparacao::TOLLS)
		return this->path->getTolls() < p.path->getTolls();
	return this->path->getTotalKms() < p.path->getTotalKms();
}

bool PPath::operator == (const PPath & p) const
{
	return (this->path->getlocTourist1() == p.path->getlocTourist1() &&
		this->path->getlocTourist2() == p.path->getlocTourist2() &&
		this->path->getPathName() == p.path->getPathName() &&
		this->path->getAvTravelTime() == p.path->getAvTravelTime() &&
		this->path->getTotalKms() == p.path->getTotalKms());
}

PPath & PPath::operator+ (const PPath & p)
{
	setKm(getKm() + p.getKm());
	setAvTime(getAvTime() + p.getAvTime());
	setTolls(getTolls() + p.getTolls());
	return *this;
}

const PPath & PPath::operator+= (const PPath & p) 
{
	*this = *this + p;
	return *this;
}

const PPath & PPath::operator= (const PPath & p)
{
	this->path = p.path->clone();
	return *this;
}
#pragma endregion

#pragma region Metodos
//apresenta a informacao sobre as auto estradas
void PPath::write(ostream & out) const
{
	out << *path << endl;
}

ostream & operator << (ostream & out, const PPath & p)
{
	p.write(out);
	return out;
}

PPath* PPath::clone() const{
	return new PPath(*this);
}
#pragma endregion

#endif PPath_