/***************************************
* Path.h
*
* Created on 10 de Outubro de 2013, 00:02
*
* @author Rui Silva & Daniela Grams
***************************************/

#ifndef Path_
#define Path_

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

#pragma region Classe Path
/* Esta classe serve para guardar a informa��o de uma Via de Liga��o
* entre dois Locais Turisticos diferentes e cont�m como atributos:
*
* - locTourist1 = variavel do tipo string que guarda o nome do Local
*				   Turistico de origem.
*
* - locTourist2 = variavel do tipo string que guarda o nome do Local
*				   Turistico de destino.
*
* - pathName = variavel do tipo string que guarda o nome da estrada.
*
* - totalKms = variavel do tipo int que guarda o numero de quilometros
*				a percorrer entre os dois Locais Turisticos referidos
*				seguinda pela estrada referida.
*
* - avTravelTime = variavel do tipo int que guarda o tempo em minutos
*					estimado para percorrer de um Local Turistico ao
*					outro pela estrada referidos.
*/

//This class cannot be iniciated.
class Path{
private:
	string locTourist1, locTourist2, pathName;
	int totalKms; //Kilometers
	int avTravelTime; //Minutes
public:
	//constructor padrao
	Path();

	//constructor normal
	Path(string lT1, string lT2, string pName, int tKms, int avTrTime);

	//constructor copia
	Path(const Path & p);

	//destructor
	virtual ~Path();

	//gets and sets
	string getlocTourist1() const;
	void setlocTourist1(string local1);

	string getlocTourist2() const;
	void setlocTourist2(string local2);

	string getPathName() const;
	void setPathName(string pName);

	int getTotalKms() const;
	void setTotalKms(int tKms);

	int getAvTravelTime() const;
	void setAvTravelTime(int avTrTime);

	virtual float getTolls() const;

	//methods
	virtual Path* clone() const = 0;
	string timeConverter() const;
	virtual void write(ostream& out) const;

	//operators
	friend ostream & operator<<(ostream & out, const Path & p);
};
#pragma endregion

#pragma region Construtores
//construtores default
Path::Path() : locTourist1(""), locTourist2(""), pathName(""), totalKms(0), avTravelTime(0)
{}

//construtor normal
Path::Path(string lT1, string lT2, string pName, int tKms, int avTrTime)
{
	locTourist1 = lT1;
	locTourist2 = lT2;
	pathName = pName;
	totalKms = tKms;
	avTravelTime = avTrTime;
}

//construtor copia
Path::Path(const Path & p)
{
	this->locTourist1 = p.locTourist1;
	this->locTourist2 = p.locTourist2;
	this->pathName = p.pathName;
	this->totalKms = p.totalKms;
	this->avTravelTime = p.avTravelTime;
}

//Destrutor
Path::~Path(){}
#pragma endregion

#pragma region Gets e Sets
//gets and sets
string Path::getlocTourist1() const
{
	return locTourist1;
}
void Path::setlocTourist1(string local1)
{
	this->locTourist1 = local1;
}

string Path::getlocTourist2() const
{
	return locTourist2;
}
void Path::setlocTourist2(string local2)
{
	this->locTourist2 = local2;
}

string Path::getPathName() const
{
	return pathName;
}
void Path::setPathName(string pName)
{
	pathName = pName;
}

int Path::getTotalKms() const
{
	return totalKms;
}
void Path::setTotalKms(int tKms)
{
	totalKms = tKms;
}

int Path::getAvTravelTime() const
{
	return avTravelTime;
}
void Path::setAvTravelTime(int avTrTime)
{
	avTravelTime = avTrTime;
}

float Path::getTolls() const 
{
	return 0;
}
#pragma endregion

#pragma region Metodos
//methods

//passa o tempo para um formato em string
string Path::timeConverter() const
{
	stringstream o;
	int hours = -1, minutes = -1;
	hours = avTravelTime / 60;
	if (hours != 0)
	{
		o << hours << " hora";
		if (hours != 1)
			o << "s";
	}
	minutes = avTravelTime % 60;
	if (hours != 0 && minutes != 0)
		o << " e ";
	if (minutes != 0)
	{
		o << minutes << " minuto";
		if (minutes != 1)
			o << "s";
	}
	return o.str();
}

//Escreve a informacao sobre uma via
void Path::write(ostream& out) const
{
	out << "Local Turistico 1: " << locTourist1 << endl;
	out << "Local Turistico 2: " << locTourist2 << endl;
	out << "Nome da estrada: " << pathName << endl;
	out << "Distancia: " << totalKms << " kms" << endl;
	out << "Tempo de viagem: " << timeConverter() << endl;
}
#pragma endregion

#pragma region Operadores
//operators
//apresenta a informacao da Path em formato String
ostream & operator << (ostream & out, const Path & p)
{
	p.write(out);
	return out;
}
#pragma endregion

#endif