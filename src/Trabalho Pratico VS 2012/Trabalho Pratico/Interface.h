/***************************************
* Interface.h
*
* Created on 24 de Outubro de 2013, 17:31
*
* @author Rui Silva & Daniela Grams
***************************************/

#ifndef Interface_
#define Interface_

#include <iostream>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

#include "MapaDigital.h"
#include "Teste.h"
#include "Util.h"

#pragma region Classe Interface
/*Esta classe � respons�vel pelo menu de intera��o
do utilizador com a aplica��o*/
class Interface
{
private:
	Teste t;
	MapaDigital mD;


public:
	//constructor e destrutor
	Interface();
	~Interface();

	//Menus
	void run();
	bool calcularPercurso();
	// 1 PARTE //
	
	//Inserir os locais de interesse turistico a partir de um ficheiro de texto com a informacao
	//estruturada do seguinte modo:
	//-Locais de interesse:
	//	NomeDoLocal,Area	-	local de interesse turistico natural
	//	NomeDoLocal,TempoMedioDeVisita,HorarioDeAbertura,HorarioDeEncerramento	-	local de interesse turistico historico
	//-Vias de ligacao:
	//	NomeLocalOrigem,NomeLocalDestino,NomeDaEstrada,TotalDeKms,TempoMedioDeViagem,TipoDePavimento	-	estrada nacional
	//	NomeLocalOrigem,NomeLocalDestino,NomeDaEstrada,TotalDeKms,TempoMedioDeViagem,PrecoPortagens	-	auto-estrada
	void importarInformacao();

	//Contabilizar os locais de interesse turistico historicos e naturais e apresenta-los por
	//ordem alfab�tica da sua descricao
	void listar();
	void locTouristNames();

	// 2 PARTE //

	//Construir o grafo a partir dos objectos anteriormente criados na classe Teste
	bool criarGrafo();

	//Determine a complexidade temporal (notacao Big-Oh) do metodo de construcao do grafo
	void complexidadeGrafo();

	//apresentar todos os percursos poss�veis entre dois locais de interesse turistico
	void todosCaminhos(PLocTourist o, PLocTourist d);

	//Calcular o percurso entre dois locais de interesse turistico:

	//mais curto em Kms
	void caminhoCurtoKms(PLocTourist o, PLocTourist d);
	//mais economico em Euros
	void caminhoCurtoEuros(PLocTourist o, PLocTourist d);
	//de maior interesse turistico(percurso que inclua o maior n�mero de locais de interesse turistico)
	void caminhoMaiorInteresse(PLocTourist o, PLocTourist d);

	bool percursoTempoInferior();

	bool percursoCompleto();

	//Dado um local de interesse tur�stico inicial, o instante de tempo de sa�da (tamb�m medido em minutos
	//a partir das 0:00) e uma lista de pontos tur�sticos a visitar, calcular o instante em que acaba a visita. Se
	//o turista chegar a um local de interesse tur�stico antes da sua abertura espera pela abertura. Se um
	//turista chegar a um local de interesse tur�stico num instante que somado ao tempo recomendado de
	//visita leva a que se ultrapasse a hora de fecho, ent�o n�o visita esse local de interesse tur�stico.
	bool construirPercurso();

	#pragma region Metodos auxiliares
	void listLocTourists();

	bool getLocTouristOrigin(PLocTourist & lT, bool histOnly = true);

	vector<PLocTourist> getLocTouristOriginDestin(bool histOnly = true);

	list<PLocTourist> getSelectedListOfLocTourists(PLocTourist lI, bool histOnly = true);
	#pragma endregion

	#pragma region Hidden signatures
	bool run2();
	bool insertInfoMenu();
	int strToInt(const string str);
	float strToFloat(const string str);
	string inputLocTourist(bool origem, string loc = "");
	string inputEstrada();
	string inputTotalKms();
	string inputTempMed();
	string inputPavimento();
	string inputTolls();
	bool insertLocTouristHistMan();
	bool insertLocTouristNatMan();
	bool insertPathMan();

	bool locTouristNames2();
	void listarLocTourist();
	void listarPaths();
	#pragma endregion
};
#pragma endregion

#pragma region Constructores
//construtor default
Interface::Interface()
{
}

//destrutor
Interface::~Interface()
{
}
#pragma endregion

#pragma region Menus
//apresenta o menu principal dando acesso aos sub menus
void Interface::run()
{
	while (1)
	{
		char choice[2];
		cout << endl << "=========== MENU PRINCIPAL ==========" << endl;
		if (t.isTesteEmpty())
			cout << mp0 << endl;
		else
		{
			cout << mp1 << endl << mp2 << endl << mp3 << endl << mp4 << endl<<mp5<<endl;
		}
		cout << mp9 << endl;
		cout << sair << endl;
		cout << op << endl;
		cout << "> ";
		cin >> choice;

		if (t.isTesteEmpty())
		{
			switch (choice[0])
			{
				case '1':
					importarInformacao();
					if (criarGrafo())
						cout << gcriado << endl;
					run();
					break;
				case '9':
					run2();
					break;
				case '0':
					system("PAUSE");
					exit(1);
				default:
					cout << endl << charinv << endl;
					break;
			}
		}
		else
		{
			vector<PLocTourist> p;
			switch (choice[0])
			{
				case '1':
					listar();
					break;
				case '2':
					//Ver a complexidade do metodo de construcao do grafo
					complexidadeGrafo();
					cout << endl;
					break;
				case '3':
					//Ver todos os percursos possiveis entre dois Locais Turisticos
					p = getLocTouristOriginDestin(false);
					if (p.size() == 2)
						mD.todosCaminhos(p.at(0), p.at(1));
					break;
				case '4':
					//Calcular percurso entre dois Locais Turisticos
					calcularPercurso();
					break;
				case '5':
					//Construir percurso
					construirPercurso();
					break;
				case '9':
					run2();
					break;
				case '0':
					system("PAUSE");
					exit(1);
				default:
					cout << endl << charinv << endl;
					break;
			}
		}		
	}
}

bool Interface::calcularPercurso()
{
	vector<PLocTourist> p = getLocTouristOriginDestin(false);
	if (p.size() == 2)
	{
		while (1)
		{
			char choice[2];
			cout << cp1 << endl;
			cout << cp2 << endl;
			cout << cp3 << endl;
			cout << mAnterior << endl;
			cout << "> ";
			cin >> choice;

			switch (choice[0]) //ao introduzir mais que um caracter d� erro! BUG!
			{
				case '1':
					mD.menorCaminhoKms(p.at(0), p.at(1));
					cout << endl;
					break;
				case '2':
					mD.menorCaminhoCusto(p.at(0), p.at(1));
					cout << endl;
					break;
				case '3':
					mD.maiorCaminhoEntre(p.at(0), p.at(1));
					cout << endl;
					break;
				case '0':
					return false;
				default:
					cout << endl << charinv << endl;
					break;
			}
		}
		return false;
	}
	return false;
}
#pragma endregion

#pragma region 1 Parte
void Interface::importarInformacao()
{
	deque<string> files;
	bool ficheiro_Unico;

	ficheiro_Unico = false;		// ler a informa��o em 2 ficheiros
	ficheiro_Unico = true;	// ler a informa��o em 1 ficheiro

	if (ficheiro_Unico)
		files.push_back("./files/Locais Turisticos e Vias de Ligacao.txt");
	else
	{
		files.push_back("./files/Locais Turisticos.txt");
		files.push_back("./files/Vias de Ligacao.txt");
	}

	stringstream resume;
	resume = t.readFiles(files);

	cout << endl << mDig << endl;
	cout << t.locTouristsSize() << lTur << endl;
	cout << "\t" << t.getNumLocTouristHist() << lHist << endl;
	cout << "\t" << t.getNumLocTouristNat() << lNat << endl;
	cout << t.vecPathsSize() << vLig << endl;
	cout << "\t" << t.getNumHighWays() << vAuto << endl;
	cout << "\t" << t.getNumNationalRoads() << vNac << endl;
	
	if (resume.str() != "")
	{
		string input;
		cout << errImp << endl;
		char myChar = { 0 };
		while (true)
		{
			cout << "> ";
			cin.sync();
			getline(cin, input);

			if (input == "")
				return;

			if (input.length() == 1) 
			{
				myChar = input[0];
				cout << resume.str() << endl;
				system("PAUSE");
				return;
			}
			cout << charinv << endl;
		}	
	}
}

void Interface::listar()
{
	//info about Mapa Digital
	cout << endl << "++++++++++++++++++++++++++++++++++++" << endl;
	if (!t.isTesteEmpty())
	{
		cout << endl << mDig << endl;
		cout << t.locTouristsSize() << lTur << endl;
		cout << "\t" << t.getNumLocTouristHist() << lHist << endl;
		cout << "\t" << t.getNumLocTouristNat() << lNat << endl;
		cout << t.vecPathsSize() << vLig << endl;
		cout << "\t" << t.getNumHighWays() << vAuto << endl;
		cout << "\t" << t.getNumNationalRoads() << vNac << endl;

		locTouristNames();
		system("PAUSE");
	}
	else
	{
		cout << nInfMDig;
	}
	cout << endl << "++++++++++++++++++++++++++++++++++++" << endl;
}

void Interface::locTouristNames()
{
	vector<string> names = t.locTouristNames();
	for (unsigned i = 0; i < names.size(); i++)
	{
		if (t.getLocTourist(names.at(i)).isLocHist())
		{
			cout << locHist << i + 1 << "'- " << names.at(i) << endl;
		}
		if (!(t.getLocTourist(names.at(i)).isLocHist()))
		{
			cout << locNat << i + 1 << "'- " << names.at(i) << endl;
		}
	}

		
}
#pragma endregion

#pragma region 2 Parte
bool Interface::criarGrafo()
{
	return mD.createGraph(t);
}

void Interface::complexidadeGrafo()
{
	int nVias, nLocais, comp;
	nVias = t.getNumHighWays() + t.getNumNationalRoads();
	nLocais = t.getNumLocTouristHist() + t.getNumLocTouristNat();
	comp = nVias*nLocais;
	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;
	cout << cCompGrafo << comp <<endl;
	cout << cGSignificado << "(" << nVias << "*" << nLocais <<")" << endl;
	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;
}

void Interface::listLocTourists()
{
	cout << "--------------------------------------------" << endl;
	deque<PLocTourist> locais = t.getLocTourists();
	//List of Tourist Places
	cout << lTur << endl;

	for each (PLocTourist x in locais)
	{
		if (x.isLocHist())
		{
			cout << locHist << x.getLocTourist().getID() << " : " << x.getLocTourist().getName() << endl;
		}
		if (!x.isLocHist())
			cout << locNat << x.getLocTourist().getID() << " : " << x.getLocTourist().getName() << endl;
	}
	cout << "--------------------------------------------" << endl;

}

bool Interface::getLocTouristOrigin(PLocTourist & lT, bool histOnly)
{
	int i = 0;
	string input;

	listLocTourists();
	
	while (true)
	{
		cout << indiqIdHOrig << endl;
		cout << "> ";
		cin.sync();
		getline(cin, input);
		if (input == "")
			return false;
		else
		{
			int aux = strToInt(input);
			if (aux != -1)
			{
				if (aux < t.locTouristsSize()+1)
				{
					if (histOnly)
					{
						if (typeid(t.getLocTourist(aux).getLocTourist()) == typeid(LocTouristHist))
						{
							lT = t.getLocTourist(aux);
							return true;
						}
						else
							cout << endl << verLocTHist << endl;
					}
					else
					{
						lT = t.getLocTourist(aux);
						return true;
					}
				}
				else
					cout << lessLocT << endl;
			}
			else
				cout << endl << notLocT << endl;
		}
	}
	return false;
}

//apresenta o formul�rio que pergunta quais Locais Turisticos (origem e destino)
//recebe como parametro um bool histOnly que se for TRUE : Locais Turisticos Historicos apenas
//se for FALSE : quaisquer tipos de Locais Turisticos
vector<PLocTourist> Interface::getLocTouristOriginDestin(bool histOnly)
{
	listLocTourists();
	vector<PLocTourist> p;
	if (t.locTouristsSize() < 2)
		cout << endl << notEnoughLocT << endl;
	else
	{
		string input;
		PLocTourist origem, destino;

		#pragma region Local de Origem
		while (true)
		{
			if (histOnly)
				cout << indiqIdHOrig << endl;
			else
				cout << indiqIdOrig << endl;
			
			cout << "> ";
			cin.sync();
			getline(cin, input);
			if (input == "")
				return p; //vector vazio
			else
			{
				int aux = strToInt(input);
				if (aux != -1)
				{
					if (aux < t.locTouristsSize() + 1)
					{
						if (histOnly)
						{
							if (t.getLocTourist(aux).isLocHist())
							{
								origem = t.getLocTourist(aux);
								break;
							}
							else
								cout << endl << verLocTHist << endl;
						}
						else
						{
							origem = t.getLocTourist(aux);
							break;
						}
					}
					else
						cout << lessLocT << endl;
				}
				else
					cout << endl << notLocT << endl;
			}
		}
		#pragma endregion

		#pragma region Local de Destino
		while (true)
		{
			if (histOnly)
				cout << indiqIdHDestino << endl;
			else
				cout << indiqIdDest << endl;
			cout << "> ";
			cin.sync();
			getline(cin, input);
			if (input == "")
			{
				p.clear();
				return p;//vector vazio
			}
			else
			{
				int aux = strToInt(input);
				if (aux != -1)
				{
					if (aux < t.locTouristsSize() + 1)
					{
						if (histOnly)
						{
							if (t.getLocTourist(aux).isLocHist())
							{
								if (t.getLocTourist(aux) != origem)
								{
									destino = t.getLocTourist(aux);
									break;
								}
								else
									cout << notAgainlocTDest << endl;
							}
							else
								cout << endl << verLocTHist << endl;
						}
						else
						{
							destino = t.getLocTourist(aux);
							break;
						}
					}
					else
						cout << lessLocT << endl;
				}
				else
					cout << endl << notLocT << endl;
			}
		}
		#pragma endregion

		p.push_back(origem);
		p.push_back(destino);
	}
	return p; //vector com dois PLocTourists (origem e destino)
}

list<PLocTourist> Interface::getSelectedListOfLocTourists(PLocTourist lI, bool histOnly)
{
	string input;
	list<PLocTourist> selectedLocTourists;
	vector<int> aux;
	while (true)
	{
		cout <<selectLocTInstruction<< endl;
		deque<PLocTourist> d = t.getLocTourists();
		for (unsigned i = 0; i < d.size(); i++)
		{
			if (d.at(i).isLocHist())
			{
				cout << "\t" << d.at(i).getLocTourist().getID() << " : " << d.at(i).getLocTourist().getName();
				if (lI == d.at(i))
					cout << LocIn;
				cout << endl;
			}
		}
		cout << "> ";
		cin.sync();
		getline(cin, input);
		if (input == "")
			return selectedLocTourists;
		else
		{
			stringstream ss(input);
			int id = -1;
			while (ss >> id)
			{
				if (id == 0)
					cout << notId0 << endl;
				else
				{
					if (id == lI.getLocTourist().getID())
					{
						cout << notIncLocal1 << lI.getLocTourist().getID()
							<< " : " << lI.getLocTourist().getName() << notPassageLocal<< notIncLocal2 << endl;
					}
					else
					{
						bool repetido = false;
						for each (int x in aux)
						{
							if (x == id)
							{
								cout << tooManyLoc << id << " (so conta uma vez)" << endl;
								repetido = true;
							}
						}
						if (!repetido)
							aux.push_back(id);
					}
				}
				if (ss.peek() == ',')
					ss.ignore();
			}
			if (id == -1)
			{
				cout << charNotRecognized << endl;
			}
			else
				break;
		}
	}
	
	for each (int x in aux)
	if (x != 0)
	{
		if (histOnly)
		{
			if (t.getLocTourist(x).isLocHist())
			{
				selectedLocTourists.push_back(t.getLocTourist(x));
			}
			else
			{
				cout << notIdLoc << x << " (\""
					<< t.getLocTourist(x).getLocTourist().getName() << "\")"<<becauseIsnt
					<< notLocTHist << endl;
			}
		}
		else
			selectedLocTourists.push_back(t.getLocTourist(x));
	}
			

	return selectedLocTourists;
}

bool Interface::percursoTempoInferior()
{
	queue < stack <PLocTourist> > caminhosPossiveis;
	int tempo;
	string input;
	vector<PLocTourist> selectedLocTourists;
	while (true)
	{
		selectedLocTourists = getLocTouristOriginDestin(false);

		if (selectedLocTourists.size() == 0)
			return false;

		while (true)
		{
			cout << howLong << endl;
			cout << "> ";
			cin.sync();
			getline(cin, input);
			if (input == "0")
			{
				cout << "Cancelado" << endl;
				return false;
			}
			else
			{
				int aux = strToInt(input);
				if (aux != -1)
				{
					if (aux > 1440)
						cout << not1day << endl;
					else
					{
						tempo = aux;
						break;
					}

				}
			}
		}

		cout << endl << "?????????????????????????????????????????????????????" << endl;
		cout << endl << locTIn << endl;
		cout << "\t" << selectedLocTourists.at(0).getLocTourist().getID() << " : " 
			<< selectedLocTourists.at(0).getLocTourist().getName() << endl;
		cout << endl << locTFim<< endl;
		cout << "\t" << selectedLocTourists.at(1).getLocTourist().getID() << " : "
			<< selectedLocTourists.at(1).getLocTourist().getName() << endl;
		cout << endl << timeAvailable << timeConverter(tempo) << endl;
		cout << endl << "?????????????????????????????????????????????????????" << endl;

		cout << endl << checkInfo << endl;

		char myChar = { 0 };

		while (true) {
			cout << "> ";
			getline(cin, input);
			if (input.length() == 1) {
				myChar = input[0];
				break;
			}
			cout << charinv << endl;
		}

		if (myChar == 'Y' || myChar == 'y')
		{
			caminhosPossiveis = 
				mD.todosOsPercursosInferior(selectedLocTourists.at(0), selectedLocTourists.at(1), tempo);
			break;
		}
		else
		{
			if (myChar != 0)
			{
				cout << "Cancelado" << endl;
				return false;
			}
		}
	}
	
	cout << pathsBetween << selectedLocTourists.at(0).getLocTourist().getName();
	cout << " e " << selectedLocTourists.at(1).getLocTourist().getName() << ": " << caminhosPossiveis.size() << endl; // Apresentado de forma invertida
	cout << andPath << endl;
	cout << "-------------------------------------------------------------" << endl;
	while (!caminhosPossiveis.empty())
	{
		stack <PLocTourist> sp = caminhosPossiveis.front();
		int tempoGasto = 0;
		while (!sp.empty()) {
			cout << sp.top().getLocTourist().getName();
			tempoGasto += sp.top().getLocTourist().getAvTime();
			if (!(sp.size() == 1))
			{
				cout << " <- ";
			}
			sp.pop();
		}
		cout <<" | " <<inTime << tempoGasto << endl;
		caminhosPossiveis.pop();
		cout << "-------------------------------------------------------------" << endl;
	}
	return false;
}

bool Interface::percursoCompleto()
{
	PLocTourist o;
	if (!getLocTouristOrigin(o, false))
	{
		cout << "Cancelado" << endl;
		return false;
	}
	
	mD.percursoCompleto(o);

	return false;
}

bool Interface::construirPercurso()
{
	queue < stack <PLocTourist> > caminhosPossiveis;
	queue <int> tempoComeco;
	queue <int> tempoVisitas;
	queue <int> tempoDeslocacao;
	queue <int> momentoSaida;
	queue <int> tempoEspera;
	PLocTourist lI;
	int tempo;
	string input;
	while (true)
	{
		if (!getLocTouristOrigin(lI))
		{
			cout << "Cancelado" << endl;
			return false;
		}
		list<PLocTourist> selectedLocTourists;
		selectedLocTourists = getSelectedListOfLocTourists(lI);
		
		if (selectedLocTourists.size() == 0)
		{
			cout << notValidCancel << endl;
			return false;
		}
		
		while (true)
		{
			cout << howLong << endl;
			cout << "> ";
			cin.sync();
			getline(cin, input);
			if (input == "0")
			{
				cout << "Cancelado" << endl;
				return false;
			}
			else
			{
				int aux = strToInt(input);
				if (aux != -1)
				{
					if (aux > 1440)
						cout << sameDay << endl;
					else
					{
						tempo = aux;
						break;
					}

				}
			}
		}

		cout << endl << "?????????????????????????????????????????????????????" << endl;
		cout << endl << locTIn << endl;
		cout << "\t" << lI.getLocTourist().getID() << " : " << lI.getLocTourist().getName() << endl;
		cout << endl << pathEndTime<< timeConverter(tempo) << endl;
		cout << endl << chosenPassLoc << endl;
		for each (PLocTourist x in selectedLocTourists)
			cout << "\t" << x.getLocTourist().getID() << " : " << x.getLocTourist().getName() << endl;
		cout << endl << "?????????????????????????????????????????????????????" << endl;

		cout << endl << checkInfo << endl;

		char myChar = { 0 };

		while (true) {
			cout << "> ";
			getline(cin, input);
			if (input.length() == 1) {
				myChar = input[0];
				break;
			}
			cout << charinv<< endl;
		}

		if (myChar == 'Y' || myChar == 'y')
		{
			caminhosPossiveis = 
				mD.viagemPersonalizada(lI, tempo, selectedLocTourists, tempoComeco, tempoVisitas, tempoDeslocacao, momentoSaida, tempoEspera);
			break;
		}
		else
		{
			if (myChar != 0)
			{
				cout << "Cancelado" << endl;
				return false;
			}
		}
	}

	if (caminhosPossiveis.empty())
		cout << cpNenc << endl;

	while (!caminhosPossiveis.empty())
	{
		cout << possiblePaths << caminhosPossiveis.size() << endl; // Apresentado de forma invertida
		cout << andPath << endl;
		cout << "-------------------------------------------------------------" << endl;
		stack <PLocTourist> caminho = caminhosPossiveis.front();

		int kms = 0, time = 0;
		float tolls = 0;
		mD.mostraCaminho(caminho, 0, 1);

		cout << endl << pathDetails << endl;
		cout << iniTime << timeConverter(tempoComeco.front()) << endl;
		cout << visitTime << timeConverter(tempoVisitas.front()) << endl;
		cout << acPathTime << timeConverter(tempoDeslocacao.front()) << endl;
		cout << exitLocTime << timeConverter(momentoSaida.front()) << endl;
		if (tempoEspera.front() != 0)
			cout << acWaitTime << timeConverter(tempoEspera.front()) << endl;
		caminhosPossiveis.pop();
		tempoComeco.pop();
		tempoVisitas.pop();
		tempoDeslocacao.pop();
		momentoSaida.pop();
		tempoEspera.pop();
		cout << "-------------------------------------------------------------" << endl;
	}
	return false;
}
#pragma endregion

#pragma region Hidden methods
//Neste menu secund�rio � possivel adicionar manualmente os quatro tipos de dados que ficam guardados em contentores
bool Interface::run2()
{
	while (1)
	{
		char choice[2];

		cout << endl << "=========== MENU AUXILIAR ==========" << endl;
		cout << mAux1 << endl;
		cout << mAux2 << endl;
		if (!t.isTesteEmpty())
		{

			cout << mAux3 << endl;
			cout << mAux4 << endl;
			if (!t.isvecPathEmpty())
			{
				cout << mAux5 << endl;
				cout << "6 - " << mAux6 << endl;
				cout << mAux7 << endl;
				cout << mAux8<< endl;
				cout << mAux9 << endl;
				cout << mAuxA << endl;
				cout << mAuxB << endl;
				cout << mAuxC << endl;
				cout << mAuxD << endl;
				cout << mAuxE << endl;
				cout << "f - " << delDados << endl;
			}
			else
			{
				cout << "5 - "<<mAux6<< endl;
				cout << "6 - "<<delDados << endl;
			}
		}
		cout << mPrinc << endl;
		cout << op << endl;
		cout << "> ";
		cin >> choice;

		if (t.isTesteEmpty())
		{
			switch (choice[0])
			{
				case '1':
					importarInformacao();
					if (criarGrafo())
						cout << gcriado << endl;
					run2();
					break;
				case '2':
					insertInfoMenu();
					run2();
					break;
				case '0':
					return false;
					break;
				default:
					cout << endl <<charinv << endl;
					break;
			}
			break;
		}
		else
		{
			if (t.isvecPathEmpty())
			{
				switch (choice[0])
				{
					case '1':
						importarInformacao();
						if (criarGrafo())
							cout << gcriado << endl;
						run2();
						break;
					case '2':
						insertInfoMenu();
						run2();
						break;
					case '3':
						locTouristNames2();
						run2();
						break;
					case '4':
						cout << endl << "====================================" << endl;
						cout << lTur<<":" << endl << endl;
						listarLocTourist();
						cout << "************************************" << endl;
						system("PAUSE");
						run2();
						break;
					case '5':
						listar();
						run2();
						break;
					case '6':
						t.clear();
						cout << endl << dadosDel << endl;
						run2();
						break;
					case '0':
						return false;
						//exit(1);
						break;
					default:
						cout << endl << charinv << endl;
						break;
				}
			}
			else
			{
				if (!t.isvecPathEmpty())
				{
					vector<PLocTourist> p;
					switch (choice[0])
					{
						
						case '1':
							importarInformacao();
							if (criarGrafo())
								cout << gcriado<< endl;
							run2();
							break;
						case '2':
							insertInfoMenu();
							run2();
							break;
						case '3':
							locTouristNames2();
							run2();
							break;
						case '4':
							cout << endl << "====================================" << endl;
							cout << lTur << ":" << endl << endl;
							listarLocTourist();
							cout << "************************************" << endl;
							system("PAUSE");
							run2();
							break;
						case '5':
							cout << endl << "====================================" << endl;
							cout << vLig << ":" << endl << endl;
							listarPaths();
							cout << "************************************" << endl;
							system("PAUSE");
							run2();
							break;
						case '6':
							listar();
							run2();
							break;
						case '7':
							//Ver dados do Grafo de Mapa Digital
							cout << endl << mD;
							run2();
							break;
						case '8':
							//Ver a complexidade do metodo de construcao do grafo
							complexidadeGrafo();
							run2();
							break;
						case '9':
							//Ver todos os percursos possiveis entre dois Locais Turisticos
							cout << "---------------------------------------------------------" << endl;
							p = getLocTouristOriginDestin(false);
							if (p.size() == 2)
								mD.todosCaminhos(p.at(0), p.at(1));
							system("PAUSE");
							cout << "---------------------------------------------------------" << endl;
							run2();
							break;
						case 'a':
							//Calcular percurso entre dois Locais Turisticos
							cout << "---------------------------------------------------------" << endl;
							calcularPercurso();
							system("PAUSE");
							cout << "---------------------------------------------------------" << endl;
							run2();
							break;
						case 'b':
							//Construir percurso
							cout << "---------------------------------------------------------" << endl;
							construirPercurso();
							system("PAUSE");
							cout << "---------------------------------------------------------" << endl;
							run2();
							break;
						case 'c':
							//Tempo necessario para visitar todos os locais
							cout << "---------------------------------------------------------" << endl;
							cout << tnecs << endl;
							cout << lIntT;
							cout << timeConverter(mD.tempoNecessarioParaTodosOsLocais()) << endl;
							system("PAUSE");
							cout << "---------------------------------------------------------" << endl;
							run2();
							break;
						case 'd':
							//Ver todos os percursos possiveis entre dois Locais Turisticos num periodo de tempo definido
							cout << "---------------------------------------------------------" << endl;
							percursoTempoInferior();
							system("PAUSE");
							cout << "---------------------------------------------------------" << endl;
							run2();
							break;
						case 'e':
							//Ver percurso completo a partir de um Local de partida
							cout << "---------------------------------------------------------" << endl;
							percursoCompleto();
							system("PAUSE");
							cout << "---------------------------------------------------------" << endl;
							run2();
							break;
						case 'f':
							t.clear();
							cout << endl << dadosDel << endl;
							run2();
							break;
						case '0':
							return false;
						default:
							cout << endl << charinv << endl;
							break;
					}
				}
				break;
			}
			break;
		}
		return true;
	}
	return false;
}

//menu para insercao de dados no mapa digital
bool Interface::insertInfoMenu()
{
	while (true)
	{
		char choice[2];
		cout << endl << "=========== INTRODUZIR MANUALMENTE DADOS NO MAPA DIGITAL ==========" << endl;
		cout << "Que informacoes pretende adicionar ao Mapa Digital?" << endl;
		cout << man1 << endl;
		cout << man2 << endl;
		cout << man3 << endl;
		cout << mAnterior << endl;
		cout << op << endl;
		cout << "> ";
		cin >> choice;

		switch (choice[0])
		{
			case '1':
				insertLocTouristNatMan();
				break;
			case '2':
				insertLocTouristHistMan();
				break;
			case '3':
				insertPathMan();
				break;
			case '0':
				return true;
			default:
				cout << endl << charinv << endl;
				break;
		}
		break;
	}
	return false;
}

// This code converts from string to integer safely.
int Interface::strToInt(const string str)
{
	int n;
	stringstream myStream(str);
	if (myStream >> n)
	{
		if (n < 1)
		{
			cout <<notLess1  << endl;
		}
		else
		{
			return n;
		}
	}
	cout << nInv<< endl;
	return -1;
}

// This code converts from string to float safely.
float Interface::strToFloat(const string str)
{
	float n;
	stringstream myStream(str);
	if (myStream >> n)
		return n;
	cout << nInv << endl;
	return -1;
}

//true se for origem | false se for destino (tem de especificar o nome do Local Turistico de origem para verificar igualdade)
string Interface::inputLocTourist(bool origem, string loc)
{
	string input;
	while (1)
	{
		if (origem)
			cout <<indiqNomeLTOrig << endl;
		else
			cout << indiqNomeLTDest << endl;
		
		cout << "> ";
		cin.sync();
		getline(cin, input);
		if (input == "")
		{
			cout << "Cancelado" << endl;
			return "";
		}
		else
		{
			if (loc != input)
			{
				if (t.isLocTourist(input))
				{
					return input;
				}
				else
				{
					cout << endl << notLocT << endl;
				}
			}
			else
				cout << endl <<notSameLoc << endl;
		}
	}
	return "";
}

string Interface::inputEstrada()
{
	string input;
	while (1)
	{
		cout << indiqNomePath << endl;
		cout << "> ";
		cin.sync();
		getline(cin, input);
		if (input == "")
		{
			cout << "Cancelado" << endl;
			return "";
		}
		else
		{
			if (input.at(0) == 'A' || input.at(0) == 'E') 
			{ 
				return input;
			}
			else
			{
				cout << invPathName << endl;
			}
		}
	}
	return "";
}

string Interface::inputTotalKms()
{
	string input;
	while (true) {
		cout << indiqTotKm << endl;
		cout << "> ";
		cin.sync();
		getline(cin, input);
		if (input == "")
		{
			cout << "Cancelado" << endl;
			return "";
		}
		else
		{
			int aux = strToInt(input);
			if (aux != -1)
			{
				if (aux != 0)
				{
					return input;
				}
			}
		}
	}
	return "";
}

string Interface::inputTempMed()
{
	string input;
	while (true) {
		cout << indiqAvgTime << endl;
		cout << "> ";
		cin.sync();
		getline(cin, input);
		if (input == "")
		{
			cout << "Cancelado" << endl;
			return "";
		}
		else
		{
			int aux = strToInt(input);
			if (aux != -1)
			{
				if (aux != 0)
				{
					return input;
				}
			}
		}
	}
	return "";
}

string Interface::inputPavimento()
{
	string input;
	while (true)
	{
		cout << indiqPavType << endl;
		cout << "> ";
		cin.sync();
		getline(cin, input);
		if (input == "")
		{
			cout << "Cancelado" << endl;
			return "";
		}
		else
		{
			return input;
		}
	}
	return "";
}

string Interface::inputTolls()
{
	string input;
	while (true)
	{
		cout << indiqTotTolls << endl;
		cout << "> ";
		cin.sync();
		getline(cin, input);
		if (input == "")
		{
			cout << "Cancelado" << endl;
			return "";
		}
		else
		{
			float aux = strToFloat(input);
			if (aux != -1)
			{
				if (aux != 0)
				{
					return input;
				}
			}
		}
	}
	return "";
}

//menu de insercao dos locais historicos de forma manual
bool Interface::insertLocTouristHistMan()
{
	stringstream resume;
	while (true)
	{
		string input;
		string name;
		string data[4];
		int media, entrada, saida;
		cin.sync();

		while (true)
		{
			cout << endl << indiqNomeLH << endl;
			cout << "> ";
			getline(cin, input);
			if (input == "")
			{
				cout << "Cancelado" << endl;
				return false;
			}
			else
			{
				if (!t.isLocTourist(input))
				{
					name = input;
					data[0] = input;
					break;
				}
				else
				{
					cout << endl << sameLT << endl;
				}
			}
		}

		while (true)
		{
			cout << endl << indiqAvgVisitTime << endl;
			cout << "> ";
			getline(cin, input);
			if (input == "")
			{
				cout << "Cancelado" << endl;
				return false;
			}
			else
			{
				int aux = strToInt(input);

				if (aux != -1)
				{
					if (!(timeConverter(aux).at(0) == '!'))
					{
						media = aux;
						data[1] = input;
						break;
					}
					else
					{
						cout << timeConverter(aux) << endl;
					}
				}
			}
		}

		while (true) 
		{
			cout << endl << indiqOpenTime << endl;
			cout << "> ";
			getline(cin, input);
			if (input == "")
			{
				cout << "Cancelado" << endl;
				return false;
			}
			else
			{
				int aux = strToInt(input);
				if (aux != -1)
				{
					if (!(timeConverter(aux).at(0) == '!'))
					{
						if (aux != 0)
						{
							if (media <= (1440 - aux))
							{
								entrada = aux;
								data[2] = input;
								break;
							}
							else
							{
								cout << endl <<  notEnoughTimeLeft<< endl;
							}
						}
						else
						{
							cout << endl << not0openTime << endl;
						}
					}
					else
					{
						cout << timeConverter(entrada) << endl;
					}
				}
			}
		}

		while (true) 
		{
			cout << endl << indiqCloseTime << endl;
			cout << "> ";
			getline(cin, input);
			if (input == "")
			{
				cout << "Cancelado" << endl;
				return false;
			}
			else
			{
				int aux = strToInt(input);
				if (aux != -1)
				{
					if (!(timeConverter(aux).at(0) == '!'))
					{
						if (aux == 0)
						{
							cout << not0closeTime << endl;
						}
						else
						{
							if (!(aux < entrada))
							{
								if (media <= (aux - entrada))
								{
									saida = aux;
									data[3] = input;
									break;
								}
								else
								{
									cout << tooSmallTimeIntv<<endl;
								}
							}
							else
							{
								cout << endl << cantCloseBeforeOpen << endl;
							}
						}
					}
					else
					{
						cout << timeConverter(saida) << endl;
					}
				}
			}
		}

		cout << endl << "?????????????????????????????????????????????????????" << endl;
		cout << locTHName << name << endl;
		cout <<avgVisitTime << timeConverter(media) << endl;
		cout << openTime << timeConverter(entrada) << endl;
		cout << closeTime << timeConverter(saida) << endl;
		cout << "?????????????????????????????????????????????????????" << endl;
		cout << endl << checkInfo << endl;
		// How to get a single char.
		char myChar = { 0 };

		while (true) 
		{
			cout << "> ";
			getline(cin, input);

			if (input.length() == 1) {
				myChar = input[0];
				break;
			}

			cout << charinv << endl;
		}
		if (myChar == 'Y' || myChar == 'y')
		{
			t.insertLocTourist(data, 6, resume);
			break;
		}
		else
		{
			if (myChar != 0)
			{
				cout << "Cancelado" << endl;
				return false;
			}
		}

	}

	if (resume.str() != "")
	{
		string input;
		cout << locTHinsertError << endl;
		char myChar = { 0 };
		while (true)
		{
			cout << "> ";
			cin.sync();
			getline(cin, input);

			if (input.length() == 1)
			{
				myChar = input[0];
				break;
			}
			cout << charinv << endl;
		}

		if (myChar == 'Y' || myChar == 'y')
			cout << resume.str() << endl;
	}
	return true;
}

//menu de insercao dos locais naturais de forma manual
bool Interface::insertLocTouristNatMan()
{
	stringstream resume;
	while (true)
	{
		string input;
		string data[2];
		string name;
		int area;
		cin.sync();
		while (true)
		{
			cout << endl << indiqNomeLN << endl;
			cout << "> ";
			getline(cin, input);
			if (input == "")
			{
				cout << "Cancelado" << endl;
				return false;
			}
			else
			{
				if (!t.isLocTourist(input))
				{
					name = input;
					data[0] = input;
					break;
				}
				else
				{
					cout << endl << sameLT << endl;
				}
			}
		}

		while (true) {
			cout << endl << indiqAreaLN << endl;
			cout << "> ";
			getline(cin, input);
			if (input == "")
			{
				cout << "Cancelado" << endl;
				return false;
			}
			else
			{
				int aux = strToInt(input);
				if (aux != -1)
				{
					if (aux != 0)
					{
						area = aux;
						data[1] = input;
						break;
					}
					else
					{
						cout << endl << not0area << endl;

					}
				}
				else
				{
					cout << invAreaNum << endl;
				}
			}
		}
		cout << endl << "?????????????????????????????????????????????????????" << endl;
		cout << locTNName<< name << endl;
		cout << areaTN << area << squareMeters << endl;
		cout << endl << checkInfo << endl;
		cout << endl << "?????????????????????????????????????????????????????" << endl;
		char myChar = { 0 };
		while (true) {
			cout << "> ";
			getline(cin, input);
			if (input.length() == 1) {
				myChar = input[0];
				break;
			}
			cout << charinv << endl;
		}

		if (myChar == 'Y' || myChar == 'y')
		{
			t.insertLocTourist(data, 2, resume);
			break;
		}
		else
		{
			if (myChar != 0)
			{
				cout << "Cancelado" << endl;
				return false;
			}
		}
	}

	if (resume.str() != "")
	{
		string input;
		cout << locTNinsertError << endl;
		char myChar = { 0 };
		while (true)
		{
			cout << "> ";
			cin.sync();
			getline(cin, input);

			if (input.length() == 1)
			{
				myChar = input[0];
				break;
			}
			cout << charinv << endl;
		}

		if (myChar == 'Y' || myChar == 'y')
			cout << resume.str() << endl;
	}
	return true;
}

//menus de insercao de vias de forma manual
bool Interface::insertPathMan()
{
	if (t.locTouristsSize() < 2)
	{
		cout << endl << notEnoughLTPaths << endl;
	}
	else
	{
		bool incorrect = true;
		stringstream resume;
		while (incorrect)
		{
			bool path; //true for HighWay //false for National Road
			string input;
			string data[6];
			cin.sync();

			data[0] = inputLocTourist(true);
			if (data[0] == "")
				return false;

			data[1] = inputLocTourist(false, data[0]);
			if (data[1] == "")
				return false;

			data[2] = inputEstrada();
			if (data[2] == "")
				return false;

			if (data[2].at(0) == 'A') path = true;
			if (data[2].at(0) == 'E') path = false;

			data[3] = inputTempMed();
			if (data[3] == "")
				return false;


			data[4] = inputTotalKms();
			if (data[4] == "")
				return false;

			if (path)
			{
				data[5] = inputTolls();
				if (data[5] == "")
					return false;
			}
			else
			{
				data[5] = inputPavimento();
				if (data[5] == "")
					return false;
			}

			cout << endl << "?????????????????????????????????????????????????????" << endl;
			cout << locTOriginName << data[0] << endl;
			cout << locTDestName << data[1] << endl;
			cout << pathName << data[2] << endl;
			cout <<totKm << strToInt(data[3]) << km << endl;
			cout << avgTravelTime << timeConverter(strToInt(data[4])) << endl;
			if (path)
			{
				cout << totToll << strToFloat(data[5]) << eur;
				if (strToFloat(data[5]) != 1)
					cout << "s" << endl;
				else
					cout << endl;
			}
			else
			{
				cout << pavType << data[5] << endl;
			}
			cout << "?????????????????????????????????????????????????????" << endl;

			cout << endl << checkInfo << endl;
			// How to get a single char.
			char myChar = { 0 };

			while (true) {
				cout << "> ";
				getline(cin, input);
				if (input.length() == 1) {
					myChar = input[0];
					break;
				}
				cout << charinv << endl;
			}

			if (myChar == 'Y' || myChar == 'y')
			{
				t.insertPath(data, resume);
				incorrect = false;
			}
			else
			{
				if (myChar != 0)
				{
					cout << "Cancelado" << endl;
					return false;
				}
			}
		}

		if (resume.str() != "")
		{
			string input;
			cout << pathInsertError << endl;
			char myChar = { 0 };
			while (true)
			{
				cout << "> ";
				cin.sync();
				getline(cin, input);

				if (input.length() == 1)
				{
					myChar = input[0];
					break;
				}
				cout << charinv << endl;
			}

			if (myChar == 'Y' || myChar == 'y')
				cout << resume.str() << endl;
		}
	}
	return true;
}

//menu para ordenar os locais turisticos por nome ou por ID
bool Interface::locTouristNames2()
{
	vector<string> names = t.locTouristNames();
	for (unsigned i = 0; i < names.size(); i++)
	{
		cout << i + 1 << "'- " << names.at(i) << endl;
	}

	while (1)
	{
		char choice[2];
		cout << "--------------------------------------------------------" << endl;
		cout << sortM1 << endl;
		cout << sortM2 << endl;
		cout << sortM0 << endl;
		cout << op << endl;
		cout << "> ";
		cin >> choice;

		switch (choice[0])
		{
			case '1':
				t.sortLocTouristsByName();
				cout << endl << lTbyName << endl;
				cout << "--------------------------------------------------------" << endl;
				return false;
			case '2':
				t.sortLocTouristsByID();
				cout << endl << lTbyId << endl;
				cout << "--------------------------------------------------------" << endl;
				return false;
			case '0':
				cout << "--------------------------------------------------------" << endl;
				return false;
			default:
				cout << endl << charinv << endl;
				break;
		}
	}
	return true;
}

//menu para listar locais turisticos
void Interface::listarLocTourist()
{
	cout << "--------------------------------------------" << endl;
	deque<PLocTourist> locais = t.getLocTourists();
	//List of Tourist Places
	cout << listLT << endl;
	for each (PLocTourist x in locais)
		cout << x.getLocTourist() << endl;
	cout << "--------------------------------------------" << endl;
}

//menu para listar vias de ligacao
void Interface::listarPaths()
{
	cout << "--------------------------------------------" << endl;
	vector<PPath> caminhos = t.getVecPaths();
	//List of vecPaths
	cout << andPath << endl;
	cout << "--------------------------------------------" << endl;
	for (unsigned i = 0; i < caminhos.size(); i++)
	{
		if (i % 3 == 0 && i != 0 && i != (caminhos.size() - 1))
		{
			system("PAUSE");
			cout << "--------------------------------------------" << endl;
		}
		cout << caminhos.at(i);
		cout << "--------------------------------------------" << endl;
	}
}
#pragma endregion
#endif