﻿/***************************************
* LocTourist.h
*
* Created on 30 de Setembro de 2013, 17:22
*
* @author Rui Silva & Daniela Grams
***************************************/

#ifndef LocTourist_
#define LocTourist_

#include <iostream>

using namespace std;


#pragma region Classe LocTourist
/*Esta classe é responsável pela construcao de locais turisticos indefinidos*/
//this class cannot be initiated
class LocTourist
{
private:
	int id;
	string name;
	//validação de id's iguais é visto em MapaDigital
public:
	//constructor padrao
	LocTourist();

	//constructor normal
	LocTourist(int id,string name);

	//constructor copia
	LocTourist(const LocTourist & lt);

	//destructor
	virtual ~LocTourist();
	
	//gets e sets
	int getID() const;
	void setID(int id);

	string getName() const;
	void setName(string n);

	virtual int getOpenTime() const;
	virtual int getCloseTime() const;
	virtual int getAvTime() const;

	//operators
	bool operator==(const LocTourist& l) const;
	bool operator!=(const LocTourist& l) const;
	bool operator>(const LocTourist& l) const;
	const LocTourist& operator=(const LocTourist& l);

	//methods
	virtual LocTourist* clone() const = 0;
	virtual void write(ostream& out) const;
};
#pragma endregion


#pragma region Construtores

//constructor padrao
LocTourist::LocTourist(){
	this->id = -1;
	this->name = '\0';
}

//constructor normal
LocTourist::LocTourist(int id, string name){
	this->id = id;
	this->name = name;
}

//constructor copia
LocTourist::LocTourist(const LocTourist & l){
	this->name = l.name;
	this->id = l.id;
}

//destructor
LocTourist::~LocTourist(){}
#pragma endregion

#pragma region Gets e Sets
//gets e sets
void LocTourist::setID(int id) {
	this->id = id;
}

int LocTourist::getID() const{
	return this->id;
}

void LocTourist::setName(string name)
{
	this->name = name;
}

string LocTourist::getName() const
{
	return this->name;
}

int LocTourist::getOpenTime() const
{
	return 0;
}

int LocTourist::getCloseTime() const
{
	return 1440;
}

int LocTourist::getAvTime() const
{
	return 0;
}
#pragma endregion

#pragma region Operadores
//Operators

//verifica se os IDs de dois locais sao iguais
bool LocTourist::operator==(const LocTourist& l) const
{
	return id == l.getID();
}

bool LocTourist::operator!=(const LocTourist& l) const
{
	return id != l.getID();
}

//verifica se o nome de um local e maior que outro alfabeticamente
bool LocTourist::operator>(const LocTourist& l) const
{
	return (this->name > l.getName());
}

//atribui a informacao de um local a outro
const LocTourist & LocTourist::operator=(const LocTourist & l)
{
	this->id = l.getID();
	this->name = l.getName();

	return *this;//l.
}

//escreve a informacao de um local em formato de string
void LocTourist::write(ostream& out) const
{
	out << "ID: " << id << endl;
	out << "Nome: " << name << endl;
}

//apresenta a informacao de um local em formato de string
ostream& operator<<(ostream& out, const LocTourist& l)
{
	l.write(out);
	return out;
}
#pragma endregion

#endif