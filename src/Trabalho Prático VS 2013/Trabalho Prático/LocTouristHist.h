﻿/***************************************
* LocTouristHist.h
*
* Created on 8 de Outubro de 2013, 14:10
*
* @author Rui Silva & Daniela Grams
***************************************/

#ifndef LocTouristHist_
#define LocTouristHist_

#include <stdio.h>
#include <iostream>
#include <sstream>
#include "LocTourist.h"

using namespace std;

#pragma region Class LocTouristHist
/*responsavel pelos locais turisticos historicos*/
class LocTouristHist: public LocTourist
{
private:
	//variaveis especificas do local turistico historico
	int avTime, openTime, closeTime;
public:
	//constructor padrao
	LocTouristHist();

	//constructor normal
	LocTouristHist(string n, int avT, int opT, int clT,int id=0);

	//constructor copia
	LocTouristHist(const LocTouristHist & lh);

	//destructor
	~LocTouristHist();

	//gets and sets
	int getAvTime() const;
	void setAvTime(int avT);

	int getOpenTime() const;
	void setOpenTime(int opT);

	int getCloseTime() const;
	void setCloseTime(int clT);

	//methods
	string timeConverter(int i) const;
	void write(ostream & out) const;
	LocTourist* clone() const;
};
#pragma endregion

#pragma region Construtores
//constructor padrao
LocTouristHist::LocTouristHist() :LocTourist()
{
	avTime = -1;
	openTime = -1;
	closeTime = -1;
}

//constructor normal
LocTouristHist::LocTouristHist(string n, int avT, int opT, int clT, int id) : LocTourist(id, n)
{
	avTime = avT;
	openTime = opT;
	closeTime = clT;
}

//constructor copia
LocTouristHist::LocTouristHist(const LocTouristHist & lh)
{
	avTime = lh.avTime;
	openTime = lh.openTime;
	closeTime = lh.closeTime;
}

//destructor
LocTouristHist::~LocTouristHist(){}
#pragma endregion

#pragma region Gets e Sets
//gets and sets
int LocTouristHist::getAvTime() const
{
	return avTime;
}

void LocTouristHist::setAvTime(int avTime)
{
	this->avTime=avTime;
}

int LocTouristHist::getOpenTime() const
{
	return openTime;
}

void LocTouristHist::setOpenTime(int openTime)
{
	this->openTime = openTime;
}

int LocTouristHist::getCloseTime() const
{
	return closeTime;
}

void LocTouristHist::setCloseTime(int closeTime)
{
	this->closeTime = closeTime;
}
#pragma endregion

#pragma region Metodos
//verifica e guarda em string o horario de funcionamento do local historico
string LocTouristHist::timeConverter(int i) const
{
	if (i > 1440)
	{
		return "!!Can't be open for more than 24 hours!!";
	}
	else
	{
		stringstream o;
		int hours = -1, minutes = -1, time = 0;
		if (i == 0) time = avTime;
		if (i == 1) time = openTime;
		if (i == 2) time = closeTime;
		hours = time / 60;
		if (hours != 0)
		{
			o << hours << " hora";
			if (hours != 1)
				o << "s";
		}
		minutes = time % 60;
		if (hours != 0 && minutes != 0)
			o << " e ";
		if (minutes != 0)
		{
			o << minutes << " minuto";
			if (minutes != 1)
				o << "s";
		}
		return o.str();
	}
}

//guarda a informacao do local historico como string
void LocTouristHist::write(ostream& out) const{
	LocTourist::write(out);
	out << "Tempo Medio de visita: " << timeConverter(0) << endl;
	out << "Horario de abertura: " << timeConverter(1) << endl; 
	out << "Horario de fecho: " << timeConverter(2) << endl;
}

//cria uma copia dum local historico
LocTourist* LocTouristHist::clone() const{
	return new LocTouristHist(*this);
}
#pragma endregion

#endif