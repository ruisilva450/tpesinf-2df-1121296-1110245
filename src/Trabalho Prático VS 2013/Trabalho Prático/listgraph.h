#ifndef ListAdjGrafo_ 
#define ListAdjGrafo_ 

#include "Vertice.h" 
#include "Ramo.h" 
#include "Stack.h" 
#include "Queue.h" 

using namespace std;

template<class TV, class TR>
class ListAdjGrafo
{
private:
	int nvertices;
	int nramos;
	Vertice<TV, TR>* graf;

	bool Caminho(Vertice <TV, TR>*v1, Vertice <TV, TR>*v2) const;
	bool Caminho(Vertice<TV, TR>* vinicio, Vertice<TV, TR>* vfim, int *vector, Stack<TV>& cam) const;
	void Caminhos(Vertice<TV, TR>* vinicio, Vertice<TV, TR>* vfim, int *vector, Stack<TV> &cam) const;

	/*********Trabalho Pratico*******************************************/
	float tempoMinimoVertice(float * tempo, bool * processados);
	float distanciaMinimoVertice(float * distancia, bool * processados);
	/********************************************************************/
	bool caminhoMinimoPesado(const TV &inicio, const TV &fim);
	bool caminhoMinimoPesado1(const TV &inicio, const TV &fim);
	void cicloCompleto(Vertice<TV, TR>* apinicio, Vertice<TV, TR>* apdest, int* visitados, Stack<TV> &cam, bool &flag) const;
	void caminhosNLigacoes(Vertice<TV, TR>* apinicio, int num, int* visitados, Stack<TV> &cam) const;
	void mostrarCaminho(int origem, int destino, const int * caminho);
	void removerIsolados();

public:
	ListAdjGrafo();
	ListAdjGrafo(const ListAdjGrafo<TV, TR>& G);
	~ListAdjGrafo();

	Vertice<TV, TR>* encvert_conteudo(const TV& v) const;
	Vertice<TV, TR>* encvert_key(int numvert) const;

	int NumVert() const;
	int NumRamos() const;

	TR MaxConteudoRamo() const;

	void juntar_vertice(const TV& vert);
	void juntar_ramo(const TR& rcont, const TV& vorigem, const TV& vdestino);

	int grau_entrada(const TV& vert) const;
	int grau_saida(const TV& vert) const;

	void visita_Largura(const TV & vinicio) const;
	void visita_Profundidade(const TV &apinicio, const int* visitados);

	void escreve_grafo() const;
	void Escreve_Caminho(TV* path, const TV& vinicio, const TV& vfim) const;

	bool Caminho(const TV &v1, const TV &v2, Stack<TV>& cam) const;
	void Caminhos(const TV &v1, const TV &v2) const;
	void caminhosNLigacoes(const TV &orig, int nLigacoes) const;
	void cicloCompleto(const TV &vert)const;
	bool Conexo() const;

	void naoAlcancaveis(const TV &orig) const;

	void Dijkstra(const TV &vi, const TV &vf) const;
	void Dijkstra(const TV &vi, const TV &vf, TV * &caminho, TR * &dist) const;
};

template<class TV, class TR>
ListAdjGrafo<TV, TR>::ListAdjGrafo()
{
	nvertices = 0;
	nramos = 0;
	graf = NULL;
}

template<class TV, class TR>
ListAdjGrafo<TV, TR>::ListAdjGrafo(const ListAdjGrafo<TV, TR>& G)
{
	Vertice<TV, TR>* apv = G.graf;
	Vertice<TV, TR>* v;
	Vertice<TV, TR>* ant;
	Ramo<TV, TR>* apr;
	Ramo<TV, TR>* prev;
	Ramo<TV, TR>* r;
	graf = NULL;
	int numvert = 0;

	while (apv)     //adiciona os vertices 
	{
		numvert++;
		Vertice<TV, TR>* vert = new Vertice<TV, TR>(apv->vconteudo, numvert);
		if (graf == NULL)
			graf = vert;
		else
		{
			v = graf;
			while (v)
			{
				ant = v;
				v = v->apvertice;
			}
			ant->apvertice = vert;
		}
		apv = apv->apvertice;
	}
	nvertices = G.nvertices;

	apv = G.graf;
	while (apv)         //adiciona os ramos   
	{
		Vertice<TV, TR>* vorig = encvert_conteudo(apv->vconteudo);
		apr = apv->apramo;
		while (apr)
		{
			Ramo<TV, TR>* ramo = new Ramo<TV, TR>(apr->rconteudo, apr->apv);

			if (vorig->apramo == NULL)
				vorig->apramo = ramo;
			else
			{
				r = vorig->apramo;
				while (r)
				{
					prev = r;
					r = r->apr;
				}
				prev->apr = ramo;
			}
			apr = apr->apr;
		}
		apv = apv->apvertice;
	}
	nramos = G.nramos;
}


template<class TV, class TR>
ListAdjGrafo<TV, TR>::~ListAdjGrafo()
{
	Vertice<TV, TR>* apv = graf;
	Vertice<TV, TR>* tempv;
	Ramo<TV, TR>* tempr;
	Ramo<TV, TR>* temp;

	while (apv)
	{
		tempr = apv->apramo;
		while (tempr)
		{
			temp = tempr;
			tempr = tempr->apr;
			delete temp;
		}
		tempv = apv;
		apv = apv->apvertice;
		delete tempv;
	}
	graf = NULL;
	nvertices = 0;
	nramos = 0;
}

template<class TV, class TR>
void ListAdjGrafo<TV, TR>::Escreve_Caminho(TV* path, const TV& vinicio, const TV& vfim) const
{
	int key_vi = encvert_conteudo(vinicio)->GetKey();
	int key_vf = encvert_conteudo(vfim)->GetKey();

	Stack <TV> st;
	st.push(vfim);
	while (key_vf != key_vi)
	{
		st.push(path[key_vf]);
		key_vf = encvert_conteudo(path[key_vf])->GetKey();
	}

	TV v;
	while
		(!st.vazia())
	{
		st.pop(v);
		cout << v << "->";
	}
	cout << "\b\b  ";
}

// Método de Dijkstra que imprime o caminho minimo entre 2 vertices. 
template<class TV, class TR>
void ListAdjGrafo<TV, TR>::Dijkstra(const TV &vi, const TV &vf) const
{
	TV *path; TR *distancias;

	Dijkstra(vi, vf, path, distancias);

	int key_vf = encvert_conteudo(vf)->GetKey();

	cout << "O caminho minimo entre " << vi << " e " << vf << " tem um custo de " << distancias[key_vf] << " e passa por: " << endl;

	Escreve_Caminho(path, vi, vf);

	delete path;
	delete distancias;
}

// Método de Dijkstra que devolve o vector path_keys com as chaves dos vertices e o vector dist com as distâncias. 
template<class TV, class TR>
void ListAdjGrafo<TV, TR>::Dijkstra(const TV &vi, const TV &vf, TV * &caminho, TR * &dist) const
{
	dist = new TR[nvertices + 1];
	Vertice<TV, TR> **path = new Vertice<TV, TR> *[nvertices + 1];
	caminho = new TV[nvertices + 1];
	int *process = new int[nvertices + 1];

	TR infinito = MaxConteudoRamo() * NumRamos();

	for (int i = 1; i < nvertices + 1; i++)
	{
		dist[i] = infinito;
		path[i] = NULL;
		process[i] = 0;
	}

	Vertice <TV, TR> *vorig = encvert_conteudo(vi);

	memset(&dist[vorig->GetKey()], 0, sizeof(dist[vorig->GetKey()])); // Inicialização com valores nulos :    dist[vorig->GetKey()] = 0; 

	while (vorig != NULL)
	{
		process[vorig->GetKey()] = 1;

		Ramo <TV, TR> *rm = vorig->apramo;
		while (rm != NULL)
		{
			Vertice <TV, TR> *vdest = rm->apv;

			if ((process[vdest->GetKey()] == 0) &&
				(dist[vdest->GetKey()] > dist[vorig->GetKey()] + rm->GetConteudo()))
			{
				dist[vdest->GetKey()] = dist[vorig->GetKey()] + rm->GetConteudo();
				path[vdest->GetKey()] = vorig;
			}

			rm = rm->apr;
		}


		int min = -1;
		for (int i = 1; i < nvertices + 1; i++)
		{
			if ((process[i] == 0) &&
				((min == -1) || (dist[i] < dist[min])))
				min = i;
		}
		if (min == -1) vorig = NULL;
		else vorig = encvert_key(min);
	}

	for (int i = 1; i<nvertices + 1; i++)
	if (path[i] != NULL) caminho[i] = path[i]->GetConteudo();

	delete process;
	delete path;
}

// Método Caminho privado de pesquisa em profundidade recursivo 
template<class TV, class TR>
bool ListAdjGrafo<TV, TR>::Caminho(Vertice <TV, TR>*v1, Vertice <TV, TR>*v2, int *vec, Stack<TV>& cam) const
{
	cam.push(v1->GetConteudo());

	vec[v1->key] = 1;
	if (v1 == v2) return true;

	Ramo <TV, TR> *ramo = v1->apramo;
	while (ramo != NULL)
	{
		if ((vec[ramo->apv->key] == 0) && Caminho(ramo->apv, v2, vec, cam)) return true;
		ramo = ramo->apr;
	}

	TV v;
	cam.pop(v);
	return false;
}


// Método Caminho privado de pesquisa em profundidade recursivo para todos os caminhos 
template<class TV, class TR>
void ListAdjGrafo<TV, TR>::Caminhos(Vertice <TV, TR>*v1, Vertice <TV, TR>*v2, int *vec, Stack<TV> &cam) const
{
	TV tmp;
	vec[v1->key] = 1;
	cam.push(v1->GetConteudo());

	Ramo <TV, TR> *ramo = v1->apramo;
	while (ramo)
	{
		if (ramo->apv == v2)
		{
			cam.push(ramo->apv->GetConteudo());
			cout << cam << endl;
			cam.pop(tmp);
		}
		else if (vec[ramo->apv->key] == 0)
			Caminhos(ramo->apv, v2, vec, cam);

		ramo = ramo->apr;
	}

	vec[v1->key] = 0;
	cam.pop(tmp);
}


// Método Caminho privado para inicializar o vector (que evita ciclos) em cada chamada ao método de pesquisa em profundidade recursivo 
template<class TV, class TR>
bool ListAdjGrafo<TV, TR>::Caminho(Vertice <TV, TR>*v1, Vertice <TV, TR>*v2) const
{
	int *vec = new int[nvertices + 1];
	for (int i = 1; i <= nvertices; i++) vec[i] = 0;
	Stack <TV> st;

	return Caminho(v1, v2, vec, st);
}

// Método existeCaminho público, recebe os vértices em TV, converte-os em apontadores de vertice e chama o método privado 
template<class TV, class TR>
bool ListAdjGrafo<TV, TR>::Caminho(const TV &v1, const TV &v2, Stack<TV>& cam) const
{
	Vertice <TV, TR> *apv1, *apv2;
	apv1 = encvert_conteudo(v1);
	apv2 = encvert_conteudo(v2);

	int *vec = new int[nvertices + 1];
	for (int i = 1; i <= nvertices; i++) vec[i] = 0;

	return Caminho(apv1, apv2, vec, cam);

	delete vec;
}

// Método existeCaminho público, recebe os vértices em TV, converte-os em apontadores de vertice e chama o método privado 
template<class TV, class TR>
void ListAdjGrafo<TV, TR>::Caminhos(const TV &v1, const TV &v2) const
{
	Vertice <TV, TR> *apv1, *apv2;
	apv1 = encvert_conteudo(v1);
	apv2 = encvert_conteudo(v2);

	int *vec = new int[nvertices + 1];
	for (int i = 1; i <= nvertices; i++) vec[i] = 0;

	Stack<TV> cam;

	Caminhos(apv1, apv2, vec, cam);

	delete vec;
}



// Método conexo, gera todos os pares de vértices e aplica a definição de conexidade 
template<class TV, class TR>
bool ListAdjGrafo<TV, TR>::Conexo() const
{
	if (graf == NULL) return true;

	Vertice <TV, TR> *v1, *v2;
	v1 = graf;

	while (v1->apvertice != NULL)
	{
		v2 = v1->apvertice;
		while (v2 != NULL)
		{
			if (!Caminho(v1, v2) && !Caminho(v2, v1))
				return false;
			v2 = v2->apvertice;
		}
		v1 = v1->apvertice;
	}

	return true;
}

template<class TV, class TR>
void ListAdjGrafo<TV, TR>::visita_Largura(const TV &vinicio) const
{
	int *visitados = new int[nvertices + 1];
	for (int i = 0; i< nvertices + 1; i++) visitados[i] = 0;
	Queue <Vertice <TV, TR> * > fila;

	cout << vinicio;
	Vertice <TV, TR> *v = encvert_conteudo(vinicio);
	visitados[v->key] = 1;
	fila.insere(v);

	while (!fila.vazia())
	{
		fila.retira(v);
		Ramo <TV, TR> *ramo = v->apramo;
		while (ramo)
		{
			int outrovert = ramo->apv->key;
			if (visitados[outrovert] == 0)
			{
				visitados[outrovert] = 1;
				cout << "  " << ramo->apv->vconteudo;
				fila.insere(ramo->apv);
			}
			ramo = ramo->apr;
		}
	}
	/**NAO TESTADO - SE ESTIVER A DAR ERRO APAGA**************/
	for (int i = 1 <= nvertices; i++){
		if (visitados[i] == 0)
			cout << encvert_key(i)->vconteudo << endl;
	}
	/*********************************************************/
	delete[] visitados;
}

/***********************COMPLETO MAS NAO TESTADO***************************************/
template<class TV, class TR>
void ListAdjGrafo<TV, TR>::visita_Profundidade(const TV &apinicio, const int* visitados){
	int *visitados = new int[nvertices + 1];
	for (int i = 0; i< nvertices + 1; i++) visitados[i] = 0;
	Queue <Vertice <TV, TR> * > fila;

	cout << vinicio;

	Ramo <TV, TR> *ramo = v->apramo;
	while (ramo){
		int outrovert = ramo->apv->key;
		if (visitados[outrovert] == 0){
			visita_Profundidade(ramo->apv, *visitados);
		}
		ramo = ramo->apv;
	}
	delete[] visitados;
}
/*****************************************************************/
/**********************NAO COMPLETO*******************************/
template<class TV, class TR>
void ListAdjGrafo<TV, TR>::cicloCompleto(Vertice<TV, TR>* apinicio, Vertice<TV, TR>* apdest, int* visitados, Stack<TV> &cam, bool &flag) const{

}

template<class TV, class TR>
void ListAdjGrafo<TV, TR>::caminhosNLigacoes(Vertice<TV, TR>* apinicio, int num, int* visitados, Stack<TV> &cam) const{

	caminhosNLigacoes(apinicio, apdest, num - 1, visitados, cam, flag);
}

template<class TV, class TR>
void ListAdjGrafo<TV, TR>::naoAlcancaveis(const TV &orig) const{

}
/****************************************************************/

template<class TV, class TR>
int ListAdjGrafo<TV, TR>::NumVert() const
{
	return nvertices;
}


template<class TV, class TR>
int ListAdjGrafo<TV, TR>::NumRamos() const
{
	return nramos;
}

template<class TV, class TR>
TR ListAdjGrafo<TV, TR>::MaxConteudoRamo() const
{
	Vertice<TV, TR>* ap = graf;
	Ramo<TV, TR>* aprmax;

	if (ap->apramo) //inicializa com o conteudo do 1º ramo  
		aprmax = ap->apramo;

	while (ap != NULL)
	{
		if (ap->apramo)   //percorre todos os ramos do vertice  
		{
			Ramo<TV, TR>* tempr = ap->apramo;
			while (tempr)
			{
				if (tempr->rconteudo > aprmax->rconteudo)
					aprmax = tempr;

				tempr = tempr->apr;
			}
		}
		ap = ap->apvertice;
	}

	return aprmax->rconteudo;
}

template<class TV, class TR>
Vertice<TV, TR>* ListAdjGrafo<TV, TR>::encvert_conteudo(const TV& v) const
{
	Vertice<TV, TR>* ap = graf;

	while (ap != NULL)
	{
		if (ap->vconteudo == v)
			return ap;
		else
			ap = ap->apvertice;
	}
	return ap;
}

template<class TV, class TR>
Vertice<TV, TR>* ListAdjGrafo<TV, TR>::encvert_key(int numvert) const
{
	Vertice<TV, TR>* ap = graf;

	while (ap != NULL)
	{
		if (ap->key == numvert)
			return ap;
		else
			ap = ap->apvertice;
	}
	return ap;
}

template<class TV, class TR>
void ListAdjGrafo<TV, TR>::juntar_vertice(const TV& vert)
{
	if (nvertices == 0)
	{
		nvertices++;
		Vertice<TV, TR>* vertice = new Vertice<TV, TR>(vert, nvertices);
		graf = vertice;
	}
	else
	{
		Vertice<TV, TR>* ap = graf;
		Vertice<TV, TR>* ant = graf;
		bool enc = false;

		while (ap != NULL && !enc)
		{
			if (ap->vconteudo == vert)
				enc = true;
			else
			{
				ant = ap;
				ap = ap->apvertice;
			}
		}
		if (!enc) //vertice nao existe 
		{
			nvertices++;
			Vertice<TV, TR>* vertice = new Vertice<TV, TR>(vert, nvertices);
			ant->apvertice = vertice;
		}
	}
}



template<class TV, class TR>
void ListAdjGrafo<TV, TR>::juntar_ramo(const TR& rcont, const TV& vorig, const TV& vdest)
{
	Ramo<TV, TR>* tempramo = NULL;
	Ramo<TV, TR>* ant;
	Vertice<TV, TR>* vertorig;
	Vertice<TV, TR>* vertdest = NULL;

	vertorig = encvert_conteudo(vorig);
	if (vertorig == NULL)
	{
		juntar_vertice(vorig);
		vertorig = encvert_conteudo(vorig);
	}
	vertdest = encvert_conteudo(vdest);
	if (vertdest == NULL)
	{
		juntar_vertice(vdest);
		vertdest = encvert_conteudo(vdest);
	}

	tempramo = vertorig->apramo;           //insere no fim da lista de ramos 
	ant = tempramo;
	while (tempramo != NULL)
	{
		ant = tempramo;
		tempramo = tempramo->apr;
	}
	if (tempramo == NULL)
	{
		tempramo = new Ramo<TV, TR>(rcont, vertdest);
		tempramo->apr = NULL;
		if (ant)
			ant->apr = tempramo;
		else
			vertorig->apramo = tempramo;

		nramos++;
	}
}



template<class TV, class TR>
int ListAdjGrafo<TV, TR>::grau_entrada(const TV& vert) const
{
	int grau = 0;
	Vertice<TV, TR>* ap = graf;
	Ramo<TV, TR>* ap1;

	while (ap)
	{
		ap1 = ap->apramo;
		while (ap1)
		{
			if (vert == ap1->apv->vconteudo)
				grau++;

			ap1 = ap1->apr;
		}
		ap = ap->apvertice;
	}
	return grau;
}


template<class TV, class TR>
int ListAdjGrafo<TV, TR>::grau_saida(const TV& vert) const
{
	Vertice<TV, TR>* ap = encvert_conteudo(vert);
	Ramo<TV, TR>* ap1;
	int grau = 0;

	if (ap->vconteudo == vert)
	{
		ap1 = ap->apramo;
		while (ap1)
		{
			grau++;
			ap1 = ap1->apr;
		}
		ap = ap->apvertice;
	}
	return grau;
}

template<class TV, class TR>
void ListAdjGrafo<TV, TR>::escreve_grafo() const
{
	Vertice<TV, TR>* v = graf;
	Ramo<TV, TR>* r;

	if (v == NULL)
		cout << "Grafo nao definido !" << endl;
	else
	{
		cout << "Num vertices " << nvertices << endl;
		cout << "Num ramos " << nramos << endl;

		while (v != NULL)
		{
			cout << "O vertice " << v->vconteudo << " liga-se a: " << endl;
			r = v->apramo;
			while (r)
			{
				cout << r->apv->vconteudo << "  ";
				cout << " Conteudo -> " << r->rconteudo << endl;
				r = r->apr;
			}
			cout << endl;
			v = v->apvertice;
		}
	}
}

template<class TV, class TR>
void ListAdjGrafo<TV, TR>::mostrarCaminho(int origem, int destino, const int * caminho){
	if (origem != destino){
		mostrarCaminho(origem, caminho[destino], caminho);
		cout << "-->";
	}
	Vertice<TV, TR> *apvert = encvert_key(destino);
	cout << destino << ":" << apvert->vconteudo;
}

template<class TV, class TR>
float ListAdjGrafo<TV, TR>::distanciaMinimoVertice(float * distancia, bool * processados){
	int minimo = INT_MAX, indVertice = -1;
	for (int i = 1; i <= nvertices; i++)
	if (!processados[i] && distancia[i]<minimo){
		minimo = distancia[i];
		indVertice = i;
	}
	return indVertice;
}

//- Caminho minimo Pesado - Algoritmo Pesado Dijkstra --- para as distancias
template<class TV, class TR>
bool ListAdjGrafo<TV, TR>::caminhoMinimoPesado(const TV &inicio, const TV &fim){
	float somadist = 0;
	bool *processados = new bool[nvertices + 1];
	int *caminho = new int[nvertices + 1];
	float *distancia = new float[nvertices + 1];
	for (int i = 1; i <= nvertices; i++){
		processados[i] = false;
		caminho[i] = 0;
		distancia[i] = 9999;
	}
	Vertice<TV, TR> *apvert = encvert_conteudo(inicio);
	Vertice<TV, TR> *apvertfim = encvert_conteudo(fim);
	if (apvert == NULL || apvertfim == NULL)
		return false;
	int indOrigem = apvert->key;
	distancia[apvert->key] = 0;
	//Este algoritmo calcula o caminho mínimo entre o vertice inicio e todos os outros
	while (indOrigem != -1){
		processados[indOrigem] = true;
		apvert = encvert_key(indOrigem);
		Ramo<TV, TR> *apramo = apvert->apramo;
		while (apramo != NULL)
		{
			int indDestino = apramo->apv->key;
			if (!processados[indDestino] && distancia[indDestino] > distancia[indOrigem] + apramo->rconteudo->getDist()){
				distancia[indDestino] = distancia[indOrigem] + apramo->rconteudo->getDist();
				caminho[indDestino] = indOrigem;
			}
			apramo = apramo->apr;
		}
		indOrigem = distanciaMinimoVertice(distancia, processados);
	}
	apvert = encvert_conteudo(inicio);
	apvertfim = encvert_conteudo(fim);
	mostrarCaminho(apvert->key, apvertfim->key, caminho);
	return true;
}

//- Caminho minimo Pesado - Algoritmo Pesado Dijkstra --- para os tempos
template<class TV, class TR>
bool ListAdjGrafo<TV, TR>::caminhoMinimoPesado1(const TV &inicio, const TV &fim){
	float somadist = 0;
	bool *processados = new bool[nvertices + 1];
	int *caminho = new int[nvertices + 1];
	float *tempo = new float[nvertices + 1];
	for (int i = 1; i <= nvertices; i++){
		processados[i] = false;
		caminho[i] = 0;
		tempo[i] = 9999;
	}
	Vertice<TV, TR> *apvert = encvert_conteudo(inicio);
	Vertice<TV, TR> *apvertfim = encvert_conteudo(fim);
	if (apvert == NULL || apvertfim == NULL)
		return false;
	int indOrigem = apvert->key;
	tempo[apvert->key] = 0;

	//Este algoritmo calcula o caminho mínimo entre o vertice inicio e todos os outros
	while (indOrigem != -1){
		processados[indOrigem] = true;
		apvert = encvert_key(indOrigem);
		Ramo<TV, TR> *apramo = apvert->apramo;
		while (apramo != NULL){
			int indDestino = apramo->apv->key;
			if (!processados[indDestino] && tempo[indDestino] > tempo[indOrigem] + apramo->rconteudo->getTemp()){
				tempo[indDestino] = tempo[indOrigem] + apramo->rconteudo->getTemp();
				caminho[indDestino] = indOrigem;
			}
			apramo = apramo->apr;
		}
		indOrigem = tempoMinimoVertice(tempo, processados);
	}
	apvert = encvert_conteudo(inicio);
	apvertfim = encvert_conteudo(fim);
	mostrarCaminho(apvert->key, apvertfim->key, caminho);
	return true;
}

template<class TV, class TR>
float ListAdjGrafo<TV, TR>::tempoMinimoVertice(float * tempo, bool * processados)
{
	int minimo = INT_MAX, indVertice = -1;
	for (int i = 1; i <= nvertices; i++)
	if (!processados[i] && tempo[i]<minimo)
	{
		minimo = tempo[i];
		indVertice = i;
	}
	return indVertice;
}

template <class TV, class TR>
void ListAdjGrafo<TV, TR>::removerIsolados(){
	Vertice<TV, TR>* ap_ant = NULL, *ap = graf;
	while (ap){
		if (grau_entrada(ap->vconteudo) == 0 && grau_saida(ap->vconteudo) == 0){
			if (ap == graf){
				graf = ap->apvertice;
				delete ap;
				ap = graf;
			}
			else{
				ap_ant->apvertice = ap->apvertice;
				delete ap;
				app = ap->apvertice;
			}
		}
		else{
			ap_ant = ap;
			ap = ap->apvertice;
		}
	}
}
#endif