#ifndef PLocTourist_
#define PLocTourist_

#include "LocTourist.h"
#include "LocTouristHist.h"
#include "LocTouristNat.h"

#pragma region Classe PLocTourist
/*classe responsavel pela juncao dos diferentes locais turisticos*/
class PLocTourist
{
private:
	LocTourist * locTourist;
public:
	PLocTourist();
	PLocTourist(const LocTourist & l);
	PLocTourist(const PLocTourist & l);
	~PLocTourist();

	//gets and sets
	void setLocTourist(LocTourist* p);
	LocTourist & getLocTourist() const;

	//operators
	bool operator>(PLocTourist & p) const;
	PLocTourist & operator+(const PLocTourist & p);
	PLocTourist & operator+=(const PLocTourist & p);
	bool operator == (const PLocTourist & p) const;
	bool operator != (const PLocTourist & p) const;
	PLocTourist & operator= (const PLocTourist & p);
	friend ostream & operator<<(ostream & out, const PLocTourist & p);

	//metodos
	bool isLocHist() const;
	void escreve(ostream & out) const;
	PLocTourist* clone() const;
};
#pragma endregion

#pragma region Construtores

PLocTourist::PLocTourist()
{
	this->locTourist = new LocTouristHist();
}

PLocTourist::PLocTourist(const LocTourist & p)
{
	this->locTourist = p.clone();
}

PLocTourist::PLocTourist(const PLocTourist & p)
{
	this->locTourist = p.locTourist;
}

PLocTourist::~PLocTourist()
{

}
#pragma endregion

#pragma region Gets e Sets
void PLocTourist::setLocTourist(LocTourist * p)
{
	locTourist = p;
}

LocTourist & PLocTourist::getLocTourist() const
{
	return *locTourist;
}
#pragma endregion

#pragma region Operadores
bool PLocTourist::operator>(PLocTourist & p) const
{
	return p.locTourist > locTourist;
}

bool PLocTourist::operator == (const PLocTourist & p) const
{
	return p.locTourist == locTourist;
}

bool PLocTourist::operator != (const PLocTourist & p) const
{
	return p.locTourist != locTourist;
}

PLocTourist & PLocTourist::operator+ (const PLocTourist & p)
{
	this->getLocTourist().setID(this->getLocTourist().getID() + p.getLocTourist().getID());
	return *this;
}

PLocTourist & PLocTourist::operator+=(const PLocTourist & p)
{
	this->getLocTourist().setID(this->getLocTourist().getID() + p.getLocTourist().getID());
	return *this;
}

PLocTourist & PLocTourist::operator= (const PLocTourist & p)
{
	this->locTourist = p.locTourist;
	return *this;
}
#pragma endregion

#pragma region Metodos

//retorna true se o apontador desta classe for do tipo LocTouristHist, ou seja,
//um Local Turistico Historico
bool PLocTourist::isLocHist() const
{
	if (typeid(*locTourist) == typeid(LocTouristHist))
		return true;
	return false;
}

//apresenta a informacao sobre o Local Turistico
void PLocTourist::escreve(ostream & out) const
{
	locTourist->write(out);
	out << endl;
}

ostream & operator << (ostream & out, const PLocTourist & p)
{
	p.escreve(out);
	return out;
}

//copia a informacao de um local natural para outro
PLocTourist* PLocTourist::clone() const{
	return new PLocTourist(*this);
}
#pragma endregion

#endif PLocTourist_